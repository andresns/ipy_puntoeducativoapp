﻿using ExternalWS.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExternalWS.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IDetalleOrdenCompra" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IDetalleOrdenCompra
    {
        [OperationContract]
        List<ExternalWS.DataContracts.DetalleOrdenCompra> GetDetalleOrdenCompra(FilterDetalleOrdenCompra filterDetalleOrdenCompra);
    }
}
