﻿using ExternalWS.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExternalWS.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "DetalleOrdenCompra" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione DetalleOrdenCompra.svc o DetalleOrdenCompra.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class DetalleOrdenCompra : IDetalleOrdenCompra
    {
        public List<ExternalWS.DataContracts.DetalleOrdenCompra> GetDetalleOrdenCompra(FilterDetalleOrdenCompra filterDetalleOrdenCompra)
        {
            List<ExternalWS.DataContracts.DetalleOrdenCompra> detalleOrdenCompras = new List<DataContracts.DetalleOrdenCompra>();
            ExternalWS.Business.Global.DetalleOrdenCompra boDetalleOrdenCompra = new Business.Global.DetalleOrdenCompra();
            ExternalWS.Entity.Global.FilterDetalleOrdenCompra fDetalleOrdenCompra = new Entity.Global.FilterDetalleOrdenCompra()
            {
                cantidad = filterDetalleOrdenCompra.Cantidad,
                precioUnitario = filterDetalleOrdenCompra.PrecioUnitario,
                idProducto = filterDetalleOrdenCompra.IdProducto,
                codOrdenCompra = filterDetalleOrdenCompra.CodOrdenCompra
            };
            List<ExternalWS.Entity.Global.DetalleOrdenCompra> detalleOrdenComprasEntity = boDetalleOrdenCompra.GetDetalleOrdenCompra(fDetalleOrdenCompra);

            if (detalleOrdenComprasEntity != null && detalleOrdenComprasEntity.Count > 0)
            {
                foreach (ExternalWS.Entity.Global.DetalleOrdenCompra doc in detalleOrdenComprasEntity)
                {
                    ExternalWS.DataContracts.DetalleOrdenCompra detalleOrdenCompra = new DataContracts.DetalleOrdenCompra()
                    {
                        Cantidad = doc.cantidad,
                        PrecioUnitario = doc.precioUnitario,
                        IdProducto = doc.idProducto,
                        CodOrdenCompra = doc.codOrdenCompra
                    };
                    detalleOrdenCompras.Add(detalleOrdenCompra);
                }
            }
            return detalleOrdenCompras;
        }
    }
}
