﻿using ExternalWS.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExternalWS.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "CabeceraOrdenCompra" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione CabeceraOrdenCompra.svc o CabeceraOrdenCompra.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class CabeceraOrdenCompra : ICabeceraOrdenCompra
    {
        public List<ExternalWS.DataContracts.CabeceraOrdenCompra> GetCabeceraOrdenCompra(FilterCabeceraOrdenCompra filterCabeceraOrdenCompra)
        {
            List<ExternalWS.DataContracts.CabeceraOrdenCompra> cabeceraOrdenCompras = new List<DataContracts.CabeceraOrdenCompra>();
            ExternalWS.Business.Global.CabeceraOrdenCompra boCabeceraOrdenCompra = new Business.Global.CabeceraOrdenCompra();
            ExternalWS.Entity.Global.FilterCabeceraOrdenCompra fCabeceraOrdenCompra = new Entity.Global.FilterCabeceraOrdenCompra()
            {
                codOrdenCompra = filterCabeceraOrdenCompra.CodOrdenCompra,
                runCliente = filterCabeceraOrdenCompra.RunCliente,
                nombreCliente = filterCabeceraOrdenCompra.NombreCliente,
                direcionCliente = filterCabeceraOrdenCompra.DireccionCliente,
                fonoCliente = filterCabeceraOrdenCompra.FonoCliente,
                emailCliente = filterCabeceraOrdenCompra.EmailCliente,
                fecha = filterCabeceraOrdenCompra.Fecha
            };
            List<ExternalWS.Entity.Global.CabeceraOrdenCompra> cabeceraOrdenComprasEntity = boCabeceraOrdenCompra.GetCabeceraOrdenCompra(fCabeceraOrdenCompra);

            if (cabeceraOrdenComprasEntity != null && cabeceraOrdenComprasEntity.Count > 0)
            {
                foreach (ExternalWS.Entity.Global.CabeceraOrdenCompra coc in cabeceraOrdenComprasEntity)
                {
                    ExternalWS.DataContracts.CabeceraOrdenCompra cabeceraOrdenCompra = new DataContracts.CabeceraOrdenCompra()
                    {
                        CodOrdenCompra = coc.codOrdenCompra,
                        RunCliente = coc.runCliente,
                        NombreCliente = coc.nombreCliente,
                        DireccionCliente = coc.direcionCliente,
                        FonoCliente = coc.fonoCliente,
                        EmailCliente = coc.emailCliente,
                        Fecha = coc.fecha
                    };
                    cabeceraOrdenCompras.Add(cabeceraOrdenCompra);
                }
            }
            return cabeceraOrdenCompras;
        }
    }
}
