﻿
DROP TABLE IF EXISTS [global].[person]
GO
CREATE TABLE [global].[person]
(
id int not null identity(1,1)
,is_deleted BIT
,is_active BIT
,is_blocked BIT
,creation_date DATETIME
,tid TIMESTAMP
 	,names VARCHAR(200)		
    ,fathers_family_name VARCHAR(200)		
    ,mothers_family_name VARCHAR(200)		
    ,email VARCHAR(200)		
    ,birth_date DATETIME		
    ,global_id VARCHAR(200)		
    , CONSTRAINT [PK_ person_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP TABLE IF EXISTS [IPY_AUDIT].[global].[person]
GO
CREATE TABLE [IPY_AUDIT].[global].[person]
(
 id int not null identity(1,1)
,person_id INT
,is_deleted BIT
,is_active BIT
,is_blocked BIT
,creation_date DATETIME
,tid TIMESTAMP
 	,names VARCHAR(200)		
    ,fathers_family_name VARCHAR(200)		
    ,mothers_family_name VARCHAR(200)		
    ,email VARCHAR(200)		
    ,birth_date DATETIME		
    ,global_id VARCHAR(200)		
    , CONSTRAINT [PK_ person_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

		
DROP PROCEDURE IF EXISTS global.get_person_by_names
GO
CREATE PROCEDURE global.get_person_by_names
@names VARCHAR(200)
	
		AS
		BEGIN
		SELECT  
				id
				,tid
				,creation_date
			,names
				,fathers_family_name
				,mothers_family_name
				,email
				,birth_date
				,global_id
								
		FROM 
				  [global].[person]
		WHERE    
			     names = @names
				 and is_deleted = 0
		END
		GO
		
DROP PROCEDURE IF EXISTS global.get_person_by_fathers_family_name
GO
CREATE PROCEDURE global.get_person_by_fathers_family_name
@fathers_family_name VARCHAR(200)
	
		AS
		BEGIN
		SELECT  
				id
				,tid
				,creation_date
			,names
				,fathers_family_name
				,mothers_family_name
				,email
				,birth_date
				,global_id
								
		FROM 
				  [global].[person]
		WHERE    
			     fathers_family_name = @fathers_family_name
				 and is_deleted = 0
		END
		GO
		
DROP PROCEDURE IF EXISTS global.get_person_by_mothers_family_name
GO
CREATE PROCEDURE global.get_person_by_mothers_family_name
@mothers_family_name VARCHAR(200)
	
		AS
		BEGIN
		SELECT  
				id
				,tid
				,creation_date
			,names
				,fathers_family_name
				,mothers_family_name
				,email
				,birth_date
				,global_id
								
		FROM 
				  [global].[person]
		WHERE    
			     mothers_family_name = @mothers_family_name
				 and is_deleted = 0
		END
		GO
		
DROP PROCEDURE IF EXISTS global.get_person_by_email
GO
CREATE PROCEDURE global.get_person_by_email
@email VARCHAR(200)
	
		AS
		BEGIN
		SELECT  
				id
				,tid
				,creation_date
			,names
				,fathers_family_name
				,mothers_family_name
				,email
				,birth_date
				,global_id
								
		FROM 
				  [global].[person]
		WHERE    
			     email = @email
				 and is_deleted = 0
		END
		GO
		
DROP PROCEDURE IF EXISTS global.get_person_by_birth_date
GO
CREATE PROCEDURE global.get_person_by_birth_date
@birth_date DATETIME
	
		AS
		BEGIN
		SELECT  
				id
				,tid
				,creation_date
			,names
				,fathers_family_name
				,mothers_family_name
				,email
				,birth_date
				,global_id
								
		FROM 
				  [global].[person]
		WHERE    
			     birth_date = @birth_date
				 and is_deleted = 0
		END
		GO
		
DROP PROCEDURE IF EXISTS global.get_person_by_global_id
GO
CREATE PROCEDURE global.get_person_by_global_id
@global_id VARCHAR(200)
	
		AS
		BEGIN
		SELECT  
				id
				,tid
				,creation_date
			,names
				,fathers_family_name
				,mothers_family_name
				,email
				,birth_date
				,global_id
								
		FROM 
				  [global].[person]
		WHERE    
			     global_id = @global_id
				 and is_deleted = 0
		END
		GO
		
DROP PROCEDURE IF EXISTS global.insert_person
GO
CREATE PROCEDURE global.insert_person
     @id INT output
	,@tid timestamp output
	,@creation_date DATETIME
 	,@names VARCHAR(200)		
    ,@fathers_family_name VARCHAR(200)		
    ,@mothers_family_name VARCHAR(200)		
    ,@email VARCHAR(200)		
    ,@birth_date DATETIME		
    ,@global_id VARCHAR(200)		
    
	AS
    BEGIN
	INSERT INTO [global].[person]	
	(
	 	names						
    ,fathers_family_name						
    ,mothers_family_name						
    ,email						
    ,birth_date						
    ,global_id						
    ,is_deleted 
	,is_active
	,is_blocked
	,creation_date
	)
	VALUES
	(
	 	@names						
    ,@fathers_family_name						
    ,@mothers_family_name						
    ,@email						
    ,@birth_date						
    ,@global_id						
    ,0
	,1
	,0
	,@creation_date
	)

	SELECT @id = SCOPE_IDENTITY(); 
	SELECT @tid = tid FROM [global].[person]	 WHERE id = @id

    INSERT INTO [IPY_AUDIT].[global].[person]	
	(
	 	names						
    ,fathers_family_name						
    ,mothers_family_name						
    ,email						
    ,birth_date						
    ,global_id						
    ,is_deleted 
	,is_active
	,is_blocked
	,creation_date
	,person_id
	)
	VALUES
	(
	 	@names						
    ,@fathers_family_name						
    ,@mothers_family_name						
    ,@email						
    ,@birth_date						
    ,@global_id						
    ,0
	,1
	,0
	,@creation_date
	,@id
	)
	
END
GO
	
DROP PROCEDURE IF EXISTS global.update_person
GO
CREATE PROCEDURE global.update_person
    @id INT
	,@creation_date DATETIME
 	,@names VARCHAR(200)		
    ,@fathers_family_name VARCHAR(200)		
    ,@mothers_family_name VARCHAR(200)		
    ,@email VARCHAR(200)		
    ,@birth_date DATETIME		
    ,@global_id VARCHAR(200)		
    	AS
    BEGIN
	UPDATE [global].[person] SET 
	 	names = @names						
    ,fathers_family_name = @fathers_family_name						
    ,mothers_family_name = @mothers_family_name						
    ,email = @email						
    ,birth_date = @birth_date						
    ,global_id = @global_id						
    ,creation_date = @creation_date
	WHERE 
	id = @id

	    INSERT INTO [IPY_AUDIT].[global].[person]	
	(
	 	names						
    ,fathers_family_name						
    ,mothers_family_name						
    ,email						
    ,birth_date						
    ,global_id						
    ,creation_date
	,person_id
	)
	VALUES
	(
	 	@names						
    ,@fathers_family_name						
    ,@mothers_family_name						
    ,@email						
    ,@birth_date						
    ,@global_id						
    	,@creation_date
	,@id
	)

END
	
GO
DROP PROCEDURE IF EXISTS global.delete_person
GO
CREATE PROCEDURE global.delete_person
	@id INT
	,@creation_date DATETIME
	AS
    BEGIN
	UPDATE [global].[person] SET is_deleted = 1, creation_date = @creation_date
	where id = @id

	INSERT INTO [IPY_AUDIT].[global].[person]	
	(
     is_deleted 
	,is_active
	,is_blocked
	,creation_date
	,person_id
	)
	VALUES
	(
	 1
	,0
	,0
	,@creation_date
	,@id
	)

END

GO
DROP PROCEDURE IF EXISTS global.get_person_by_id
GO
CREATE PROCEDURE global.get_person_by_id
	@id INT
	AS
    BEGIN
	SELECT 
			id
			,tid
			,creation_date
			,names
				,fathers_family_name
				,mothers_family_name
				,email
				,birth_date
				,global_id
					FROM 
	[global].[person] 
	WHERE 
		id = @id AND 
		is_deleted = 0

END

GO
DROP PROCEDURE IF EXISTS global.get_person_all
GO
CREATE PROCEDURE global.get_person_all
	AS
    BEGIN
	SELECT 
			id
			,tid
			,creation_date
			,names
				,fathers_family_name
				,mothers_family_name
				,email
				,birth_date
				,global_id
					FROM 
	[global].[person] 
	WHERE 
		is_deleted = 0

END
GO
DROP PROCEDURE IF EXISTS global.get_person_by_dynamic_parameters
GO
CREATE PROCEDURE global.get_person_by_dynamic_parameters
 @id INT
 	,@names VARCHAR(200)		
    ,@fathers_family_name VARCHAR(200)		
    ,@mothers_family_name VARCHAR(200)		
    ,@email VARCHAR(200)		
    ,@birth_date DATETIME		
    ,@global_id VARCHAR(200)		
    	AS
    BEGIN
SELECT 
			id
			,tid
			,creation_date
			,names
				,fathers_family_name
				,mothers_family_name
				,email
				,birth_date
				,global_id
				FROM 
	[global].[person] 
	WHERE 
	is_deleted = 0
	AND (names = CASE WHEN @names IS NOT NULL THEN @names ELSE names END OR names IS NULL)  		
    AND (fathers_family_name = CASE WHEN @fathers_family_name IS NOT NULL THEN @fathers_family_name ELSE fathers_family_name END OR fathers_family_name IS NULL)  		
    AND (mothers_family_name = CASE WHEN @mothers_family_name IS NOT NULL THEN @mothers_family_name ELSE mothers_family_name END OR mothers_family_name IS NULL)  		
    AND (email = CASE WHEN @email IS NOT NULL THEN @email ELSE email END OR email IS NULL)  		
    AND (birth_date = CASE WHEN @birth_date IS NOT NULL THEN @birth_date ELSE birth_date END OR birth_date IS NULL)  		
    AND (global_id = CASE WHEN @global_id IS NOT NULL THEN @global_id ELSE global_id END OR global_id IS NULL)  		
    	
END
