﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExternalWS.GUI.ServicioWeb;

namespace ExternalWS.GUI
{
    public partial class PantallaModificarProducto : Form
    {
        private ServicioWeb.Producto producto;
        private PantallaInicio pInicio;

        public Producto Producto { get => producto; set => producto = value; }
        public PantallaInicio PInicio { get => pInicio; set => pInicio = value; }

        public PantallaModificarProducto(ServicioWeb.Producto producto, PantallaInicio pInicio)
        {
            InitializeComponent();
            this.Producto = producto;
            this.PInicio = pInicio;
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
            System.GC.Collect();
        }

        private void PantallaModificarProducto_Load(object sender, EventArgs e)
        {
            txtIdProducto.Text = this.Producto.IdProducto.ToString();
            txtDescripcionProducto.Text = this.Producto.Descripcion;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ServicioWeb.ProductoClient wsProducto = new ServicioWeb.ProductoClient();

            ExternalWS.Entity.Global.Producto producto = new Entity.Global.Producto();
            producto.idProducto = int.Parse(txtIdProducto.Text);
            producto.descripcion = txtDescripcionProducto.Text;
            wsProducto.modificarProducto(producto);

            MessageBox.Show("Se han guardado los cambios.", "Sistema");

            this.Dispose();
            this.PInicio.cargarTabla();
            System.GC.Collect();
        }
    }
}
