﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Business.Global
{
    public class CabeceraOrdenCompra
    {

        public void SaveCabeceraOrdenCompra(ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra, int staffMemberIdCreate)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            mt.BeginTransaction();
            try
            {
                IData.IGlobal dalCabeceraOrdenCompra = Factory.Global.Create();

                switch (Utils.Utils.ChooseSaveAction(cabeceraOrdenCompra))
                {
                    case Utils.Utils.SaveActions.Create:
                        if (Utils.Utils.NeedSave(cabeceraOrdenCompra))
                        {
                            dalCabeceraOrdenCompra.InsertCabeceraOrdenCompra(cabeceraOrdenCompra);
                        }
                        break;
                    case Utils.Utils.SaveActions.Update:
                        if (Utils.Utils.NeedSave(cabeceraOrdenCompra))
                        {
                            dalCabeceraOrdenCompra.UpdateCabeceraOrdenCompra(cabeceraOrdenCompra);
                        }
                        break;

                    case Utils.Utils.SaveActions.Delete:
                        if (Utils.Utils.NeedSave(cabeceraOrdenCompra))
                        {
                            dalCabeceraOrdenCompra.DeleteCabeceraOrdenCompra(cabeceraOrdenCompra);
                        }
                        break;
                    default:
                        break;


                }
                mt.AcceptTransaction();
            }
            catch (Exception e)
            {
                mt.RollBackTransaction();
                throw e;
            }
        }

        public List<ExternalWS.Entity.Global.CabeceraOrdenCompra> GetCabeceraOrdenCompra(ExternalWS.Entity.Global.FilterCabeceraOrdenCompra filterCabeceraOrdenCompra)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalCabeceraOrdenCompra = Factory.Global.Create();
            List<ExternalWS.Entity.Global.CabeceraOrdenCompra> cabeceraOrdenCompraList = dalCabeceraOrdenCompra.GetCabeceraOrdenCompra(filterCabeceraOrdenCompra);
            return cabeceraOrdenCompraList;
        }

        public List<ExternalWS.Entity.Global.CabeceraOrdenCompra> GetCabeceraOrdenCompra()
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalCabeceraOrdenCompra = Factory.Global.Create();
            List<ExternalWS.Entity.Global.CabeceraOrdenCompra> cabeceraOrdenCompraList = dalCabeceraOrdenCompra.GetCabeceraOrdenCompra();
            return cabeceraOrdenCompraList;
        }

        public ExternalWS.Entity.Global.CabeceraOrdenCompra GetCabeceraOrdenCompra(int id)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalCabeceraOrdenCompra = Factory.Global.Create();
            ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompraList = dalCabeceraOrdenCompra.GetCabeceraOrdenCompra(id);
            return cabeceraOrdenCompraList;
        }
    }
}