﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Utils
{
    public static class Constants
    {
        public const string ORIGINS = "*";
        public const string HEADERS = "*";
        public const string METHODS = "*";
        public const string WILDCARD = "*";

        public const string TOKEN_PREFIX = "SDX ";

        public const string HTML_DOCTYPE = "text/html";
        public const string JSON_DOCTYPE = "application/json";

        public const string HOST = "http://localhost:80";
        public const string API_NODE = "api/id/{idid}/";

        public const string GENERAL_PRINT_DOCUMENTS = "";



    }
}
