﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace ExternalWS.DataContracts
{
    [DataContract]
    public class Person
    {
        DateTime _birthDay;
        string _names = "";
        string _fathersFamilyName = "";
        string _mothersFamilyName = "";
        string _email = "";
        string _globalId = "";

        [DataMember]
        public DateTime BirthDate
        {
            get { return _birthDay; }
            set { _birthDay = value; }
        }

        [DataMember]
        public string Names
        {
            get { return _names; }
            set { _names = value; }
        }

        [DataMember]
        public string FathersFamilyName
        {
            get { return _fathersFamilyName; }
            set { _fathersFamilyName = value; }
        }

        [DataMember]
        public string MothersFamilyName
        {
            get { return _mothersFamilyName; }
            set { _mothersFamilyName = value; }
        }

        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        [DataMember]
        public string GlobalId
        {
            get { return _globalId; }
            set { _globalId = value; }
        }
    }

    [DataContract]
    public class FilterPerson
    {
        DateTime _birthDay;
        string _names = "";
        string _fathersFamilyName = "";
        string _mothersFamilyName = "";
        string _email = "";
        string _globalId = "";

        [DataMember]
        public DateTime BirthDate
        {
            get { return _birthDay; }
            set { _birthDay = value; }
        }

        [DataMember]
        public string Names
        {
            get { return _names; }
            set { _names = value; }
        }

        [DataMember]
        public string FathersFamilyName
        {
            get { return _fathersFamilyName; }
            set { _fathersFamilyName = value; }
        }

        [DataMember]
        public string MothersFamilyName
        {
            get { return _mothersFamilyName; }
            set { _mothersFamilyName = value; }
        }

        [DataMember]
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        [DataMember]
        public string GlobalId
        {
            get { return _globalId; }
            set { _globalId = value; }
        }
    }
}
