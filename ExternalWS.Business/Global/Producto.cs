﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Business.Global
{
    public class Producto
    {

        public void SaveProducto(ExternalWS.Entity.Global.Producto producto)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            mt.BeginTransaction();
            try
            {
                IData.IGlobal dalProducto = Factory.Global.Create();

                switch (Utils.Utils.ChooseSaveAction(producto))
                {
                    case Utils.Utils.SaveActions.Create:
                        if (Utils.Utils.NeedSave(producto))
                        {
                            dalProducto.InsertProducto(producto);
                        }
                        break;
                    case Utils.Utils.SaveActions.Update:
                        if (Utils.Utils.NeedSave(producto))
                        {
                            dalProducto.UpdateProducto(producto);
                        }
                        break;

                    case Utils.Utils.SaveActions.Delete:
                        if (Utils.Utils.NeedSave(producto))
                        {
                            dalProducto.DeleteProducto(producto);
                        }
                        break;
                    default:
                        break;


                }
                mt.AcceptTransaction();
            }
            catch (Exception e)
            {
                mt.RollBackTransaction();
                throw e;
            }
        }

        public List<ExternalWS.Entity.Global.Producto> GetProducto(ExternalWS.Entity.Global.FilterProducto filterProducto)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalProducto = Factory.Global.Create();
            List<ExternalWS.Entity.Global.Producto> productoList = dalProducto.GetProducto(filterProducto);
            return productoList;
        }

        public List<ExternalWS.Entity.Global.Producto> GetProducto()
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalProducto = Factory.Global.Create();
            List<ExternalWS.Entity.Global.Producto> productoList = dalProducto.GetProducto();
            return productoList;
        }

        public ExternalWS.Entity.Global.Producto GetProducto(int id)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalProducto = Factory.Global.Create();
            ExternalWS.Entity.Global.Producto productoList = dalProducto.GetProducto(id);
            return productoList;
        }
    }
}