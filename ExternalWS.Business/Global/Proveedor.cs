﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Business.Global
{
    public class Proveedor
    {

        public void SaveProveedor(ExternalWS.Entity.Global.Proveedor proveedor, int staffMemberIdCreate)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            mt.BeginTransaction();
            try
            {
                IData.IGlobal dalProveedor = Factory.Global.Create();

                switch (Utils.Utils.ChooseSaveAction(proveedor))
                {
                    case Utils.Utils.SaveActions.Create:
                        if (Utils.Utils.NeedSave(proveedor))
                        {
                            dalProveedor.InsertProveedor(proveedor);
                        }
                        break;
                    case Utils.Utils.SaveActions.Update:
                        if (Utils.Utils.NeedSave(proveedor))
                        {
                            dalProveedor.UpdateProveedor(proveedor);
                        }
                        break;

                    case Utils.Utils.SaveActions.Delete:
                        if (Utils.Utils.NeedSave(proveedor))
                        {
                            dalProveedor.DeleteProveedor(proveedor);
                        }
                        break;
                    default:
                        break;


                }
                mt.AcceptTransaction();
            }
            catch (Exception e)
            {
                mt.RollBackTransaction();
                throw e;
            }
        }

        public List<ExternalWS.Entity.Global.Proveedor> GetProveedor(ExternalWS.Entity.Global.FilterProveedor filterProveedor)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalProveedor = Factory.Global.Create();
            List<ExternalWS.Entity.Global.Proveedor> proveedorList = dalProveedor.GetProveedor(filterProveedor);
            return proveedorList;
        }

        public List<ExternalWS.Entity.Global.Proveedor> GetProveedor()
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalProveedor = Factory.Global.Create();
            List<ExternalWS.Entity.Global.Proveedor> proveedorList = dalProveedor.GetProveedor();
            return proveedorList;
        }

        public ExternalWS.Entity.Global.Proveedor GetProveedor(int id)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalProveedor = Factory.Global.Create();
            ExternalWS.Entity.Global.Proveedor proveedorList = dalProveedor.GetProveedor(id);
            return proveedorList;
        }
    }
}