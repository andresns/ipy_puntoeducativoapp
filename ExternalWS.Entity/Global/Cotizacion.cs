﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Entity.Global
{
    public class Cotizacion
    {
        public CabeceraCotizacion cabeceraCotizacion { get; set; }
        public List<DetalleCotizacion> listaDetalleCotizacion { get; set; }
    }
}
