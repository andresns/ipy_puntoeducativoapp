﻿using ExternalWS.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Business.Utils
{
    public static class Utils
    {
        #region Metodo ChooseSaveAction()
        public static SaveActions ChooseSaveAction(IPersistentBase entity)
        {
            if (!NeedSave(entity))
                return SaveActions.Nothing;
            if (entity.IsNew)
                return SaveActions.Create;
            if (entity.IsDeleted)
                return SaveActions.Delete;
            return SaveActions.Update;
        }
        #endregion

        #region Metodo NeedSave()
        public static bool NeedSave(IPersistentBase entity)
        {
            if (entity.IsNew && entity.IsDeleted)
                return false;
            if (!entity.IsNew && !entity.IsDeleted && !entity.IsModified)
                return false;
            return true;
        }
        #endregion

        #region Enum SaveActions
        public enum SaveActions
        {
            Nothing,
            Create,
            Update,
            Delete
        }
        #endregion
    }
}
