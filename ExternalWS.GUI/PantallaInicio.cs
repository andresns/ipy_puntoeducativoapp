﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExternalWS.Services;
using ExternalWS.Entity;

namespace ExternalWS.GUI
{
    public partial class PantallaInicio : Form
    {
        public PantallaInicio()
        {
            InitializeComponent();
        }

        private void PantallaInicio_Load(object sender, EventArgs e)
        {
            btnModificar.Enabled = false;
            btnEliminar.Enabled = false;

            cargarTabla();
        }

        public void cargarTabla()
        {
            DataTable auxDataTable = new DataTable();
            auxDataTable.Columns.Add("Id", typeof(int));
            auxDataTable.Columns.Add("Descripcion", typeof(string));

            dgTablaResultados.DataSource = auxDataTable;

            ServicioWeb.ProductoClient wsProducto = new ServicioWeb.ProductoClient();

            ServicioWeb.FilterProducto wsFilterProducto = new ServicioWeb.FilterProducto();
            wsFilterProducto.SystemName = "user";
            wsFilterProducto.Password = "pass";

            

            ServicioWeb.ResponseParameterProducto wsResponseProducto = new ServicioWeb.ResponseParameterProducto();

            wsResponseProducto = wsProducto.GetProducto(wsFilterProducto);

            List<ServicioWeb.Producto> productos = new List<ServicioWeb.Producto>();

            productos = wsResponseProducto.Productos.ToList();

            foreach (ServicioWeb.Producto p in productos)
            {
                DataRow auxDataRow = auxDataTable.NewRow();
                auxDataRow["Id"] = p.IdProducto;
                auxDataRow["Descripcion"] = p.Descripcion;
                auxDataTable.Rows.Add(auxDataRow);
            }

            dgTablaResultados.Columns["Id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgTablaResultados.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgTablaResultados.Columns["Id"].ReadOnly = true;
            dgTablaResultados.Columns["Descripcion"].ReadOnly = true;

            dgTablaResultados.ClearSelection();
        }


        #region Botones

        private void BtnCrearProducto_Click(object sender, EventArgs e)
        {
            if (txtIdProducto.Text == string.Empty || txtDescripcionProducto.Text == string.Empty)
            {
                MessageBox.Show("Debe completar todos los campos", "Sistema");
            }
            else if (int.Parse(txtIdProducto.Text)<1)
            {
                MessageBox.Show("El Id del producto debe ser mayor a 0", "Sistema");   
            }
            else
            {
                ServicioWeb.ProductoClient wsProducto = new ServicioWeb.ProductoClient();

                ExternalWS.Entity.Global.Producto producto = new Entity.Global.Producto();
                producto.idProducto = int.Parse(txtIdProducto.Text);
                producto.descripcion = txtDescripcionProducto.Text;
                wsProducto.ingresarProducto(producto);
                cargarTabla();

                MessageBox.Show("El producto ha sido creado", "Sistema");
            }
            
        }

        private void BtnBuscar_Click(object sender, EventArgs e)
        {
            DataTable auxDataTable = new DataTable();
            auxDataTable.Columns.Add("Id", typeof(int));
            auxDataTable.Columns.Add("Descripcion", typeof(string));

            dgTablaResultados.DataSource = auxDataTable;

            ServicioWeb.ProductoClient wsProducto = new ServicioWeb.ProductoClient();

            ServicioWeb.FilterProducto wsFilterProducto = new ServicioWeb.FilterProducto();
            wsFilterProducto.SystemName = "user";
            wsFilterProducto.Password = "pass";
            wsFilterProducto.Descripcion = txtBuscarDescripcion.Text;

            ServicioWeb.ResponseParameterProducto wsResponseProducto = new ServicioWeb.ResponseParameterProducto();

            wsResponseProducto = wsProducto.GetProducto(wsFilterProducto);

            List<ServicioWeb.Producto> productos = new List<ServicioWeb.Producto>();

            productos = wsResponseProducto.Productos.ToList();

            foreach (ServicioWeb.Producto p in productos)
            {
                DataRow auxDataRow = auxDataTable.NewRow();
                auxDataRow["Id"] = p.IdProducto;
                auxDataRow["Descripcion"] = p.Descripcion;
                auxDataTable.Rows.Add(auxDataRow);
            }

            dgTablaResultados.Columns["Id"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            dgTablaResultados.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dgTablaResultados.Columns["Id"].ReadOnly = true;
            dgTablaResultados.Columns["Descripcion"].ReadOnly = true;

            dgTablaResultados.ClearSelection();
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            int indiceFila = dgTablaResultados.SelectedCells[0].RowIndex;
            DataGridViewRow filaSeleccionada = dgTablaResultados.Rows[indiceFila];

            int idProducto = int.Parse(filaSeleccionada.Cells["Id"].Value.ToString());
            string descProducto = filaSeleccionada.Cells["Descripcion"].Value.ToString();

            ServicioWeb.Producto producto = new ServicioWeb.Producto();

            producto.IdProducto = idProducto;
            producto.Descripcion = descProducto;

            PantallaModificarProducto pModificarProducto = new PantallaModificarProducto(producto, this);
            pModificarProducto.ShowDialog();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("¿Está seguro de eliminar el producto?", "Confirmación", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                int indiceFila = dgTablaResultados.SelectedCells[0].RowIndex;
                DataGridViewRow filaSeleccionada = dgTablaResultados.Rows[indiceFila];

                int idProducto = int.Parse(filaSeleccionada.Cells["Id"].Value.ToString());
                string descProducto = filaSeleccionada.Cells["Descripcion"].Value.ToString();

                ServicioWeb.ProductoClient wsProducto = new ServicioWeb.ProductoClient();

                Entity.Global.Producto producto = new Entity.Global.Producto();
                producto.idProducto = idProducto;
                producto.descripcion = descProducto;

                wsProducto.eliminarProducto(producto);
                cargarTabla();

                MessageBox.Show("El Producto ha sido eliminado.", "Sistema");
            }
        }

        #endregion Botones

        #region Eventos GUI

        private void DgTablaResultados_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            btnModificar.Enabled = true;
            btnEliminar.Enabled = true;
        }

        private void TxtIdProducto_Enter(object sender, EventArgs e)
        {
            dgTablaResultados.ClearSelection();
        }

        private void TxtDescripcionProducto_Enter(object sender, EventArgs e)
        {
            dgTablaResultados.ClearSelection();
        }

        private void TxtBuscarProducto_Enter(object sender, EventArgs e)
        {
            dgTablaResultados.ClearSelection();
        }

        #endregion  Eventos GUI
    }
}
