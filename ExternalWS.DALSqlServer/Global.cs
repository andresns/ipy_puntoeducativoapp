﻿using System;
using System.Collections.Generic;
using System.Data;
using ExternalWS.IData;
using ExternalWS.Utils;
using System.Runtime.Caching;
using ExternalWS.Entity.Global;

namespace ExternalWS.DALSQLServer
{
    public partial class Global : IGlobal
    {

        #region Constants
        private const string CACHE_NAME = "PERSON";
        private const string SP_INSERT_PERSON = "Global.insert_person";
        private const string SP_UPDATE_PERSON = "Global.update_person";
        private const string SP_DELETE_PERSON = "Global.delete_person";
        private const string PARAM_ID = "Id";
        private const string PARAM_OID = "Id";
        private const string PARAM_TIMESTAMP = "tid";
        private const string PARAM_OTIMESTAMP = "o_tid";
        private const string PARAM_IS_DELETED = "is_deleted";
        private const string PARAM_IS_ACTIVE = "is_active";
        private const string GET_PERSON_ALL = "Global.get_person_all";
        private const string GET_PERSON_BY_ID = "Global.get_person_by_id";
        private const string GET_PERSON_BY_DYNAMIC_PARAMETERS = "Global.get_person_by_dynamic_parameters";
        private const string GET_PERSON_BY_NAMES = "Global.get_person_by_names";
        private const string GET_PERSON_BY_FATHERS_FAMILY_NAME = "Global.get_person_by_fathers_family_name";
        private const string GET_PERSON_BY_MOTHERS_FAMILY_NAME = "Global.get_person_by_mothers_family_name";
        private const string GET_PERSON_BY_EMAIL = "Global.get_person_by_email";
        private const string GET_PERSON_BY_BIRTH_DATE = "Global.get_person_by_birth_date";
        private const string GET_PERSON_BY_GLOBAL_ID = "Global.get_person_by_global_id";
        #endregion

        #region InsertPerson(ExternalWS.Entity.Global.Person person)
        public void InsertPerson(ExternalWS.Entity.Global.Person person)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_INSERT_PERSON);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);


            cmd.AddInParameter("names", DbType.String, person.Names);
            cmd.AddInParameter("fathers_family_name", DbType.String, person.FathersFamilyName);
            cmd.AddInParameter("mothers_family_name", DbType.String, person.MothersFamilyName);
            cmd.AddInParameter("email", DbType.String, person.Email);
            cmd.AddInParameter("birth_date", DbType.DateTime, person.BirthDate == DateTime.MinValue ? null : (object)person.BirthDate);
            cmd.AddInParameter("global_id", DbType.String, person.GlobalId); cmd.AddOutParameter(PARAM_OID, DbType.Int32, 0);
            cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                person.Id = (int)cmd.GetParameterValue(PARAM_OID);
                person.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region UpdatePerson(ExternalWS.Entity.Global.Person person)
        public void UpdatePerson(ExternalWS.Entity.Global.Person person)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_UPDATE_PERSON);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);

            cmd.AddInParameter(PARAM_ID, DbType.Int32, person.Id);
            cmd.AddInParameter("names", DbType.String, person.Names);
            cmd.AddInParameter("fathers_family_name", DbType.String, person.FathersFamilyName);
            cmd.AddInParameter("mothers_family_name", DbType.String, person.MothersFamilyName);
            cmd.AddInParameter("email", DbType.String, person.Email);
            cmd.AddInParameter("birth_date", DbType.DateTime, person.BirthDate == DateTime.MinValue ? null : (object)person.BirthDate);
            cmd.AddInParameter("global_id", DbType.String, person.GlobalId);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(person.TimeStamp as string));
            //cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                //person.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region DeletePerson(ExternalWS.Entity.Global.Person person)
        public void DeletePerson(ExternalWS.Entity.Global.Person person)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_DELETE_PERSON);
            cmd.AddInParameter(PARAM_ID, DbType.Int32, person.Id);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);
            //cmd.AddInParameter(PARAM_IS_DELETED, DbType.Boolean, 1);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(person.TimeStamp as string));
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                mt.AcceptTransaction();
            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region GetPerson(ExternalWS.Entity.Global.FilterPerson filterPerson)
        public List<ExternalWS.Entity.Global.Person> GetPerson(ExternalWS.Entity.Global.FilterPerson filterPerson)
        {
            List<ExternalWS.Entity.Global.Person> personList = new List<ExternalWS.Entity.Global.Person>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = null;
            PersonFilterType filterType = GetPersonFilterType(filterPerson);
            switch (filterType)
            {
                case PersonFilterType.None:
                    throw new Exception("Empty Filter");
                case PersonFilterType.Names:
                    cmd = mt.CreateCommand(GET_PERSON_BY_NAMES);
                    cmd.AddInParameter("names", DbType.String, filterPerson.Names);
                    break;

                case PersonFilterType.FathersFamilyName:
                    cmd = mt.CreateCommand(GET_PERSON_BY_FATHERS_FAMILY_NAME);
                    cmd.AddInParameter("fathers_family_name", DbType.String, filterPerson.FathersFamilyName);
                    break;

                case PersonFilterType.MothersFamilyName:
                    cmd = mt.CreateCommand(GET_PERSON_BY_MOTHERS_FAMILY_NAME);
                    cmd.AddInParameter("mothers_family_name", DbType.String, filterPerson.MothersFamilyName);
                    break;

                case PersonFilterType.Email:
                    cmd = mt.CreateCommand(GET_PERSON_BY_EMAIL);
                    cmd.AddInParameter("email", DbType.String, filterPerson.Email);
                    break;

                case PersonFilterType.BirthDate:
                    cmd = mt.CreateCommand(GET_PERSON_BY_BIRTH_DATE);
                    cmd.AddInParameter("birth_date", DbType.DateTime, filterPerson.BirthDate == DateTime.MinValue ? null : (object)filterPerson.BirthDate);
                    break;

                case PersonFilterType.GlobalId:
                    cmd = mt.CreateCommand(GET_PERSON_BY_GLOBAL_ID);
                    cmd.AddInParameter("global_id", DbType.String, filterPerson.GlobalId);
                    break;

                default:
                    cmd = mt.CreateCommand(GET_PERSON_BY_DYNAMIC_PARAMETERS);
                    cmd.AddInParameter("@id", DbType.Int32, 0);

                    cmd.AddInParameter("names", DbType.String, filterPerson.Names == "" ? null : filterPerson.Names);
                    cmd.AddInParameter("fathers_family_name", DbType.String, filterPerson.FathersFamilyName == "" ? null : filterPerson.FathersFamilyName);
                    cmd.AddInParameter("mothers_family_name", DbType.String, filterPerson.MothersFamilyName == "" ? null : filterPerson.MothersFamilyName);
                    cmd.AddInParameter("email", DbType.String, filterPerson.Email == "" ? null : filterPerson.Email);
                    cmd.AddInParameter("birth_date", DbType.DateTime, filterPerson.BirthDate == DateTime.MinValue ? null : (object)filterPerson.BirthDate);
                    cmd.AddInParameter("global_id", DbType.String, filterPerson.GlobalId == "" ? null : filterPerson.GlobalId); break;
            }

            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int NAMES = rdr.GetOrdinal("names");
                    int FATHERS_FAMILY_NAME = rdr.GetOrdinal("fathers_family_name");
                    int MOTHERS_FAMILY_NAME = rdr.GetOrdinal("mothers_family_name");
                    int EMAIL = rdr.GetOrdinal("email");
                    int BIRTH_DATE = rdr.GetOrdinal("birth_date");
                    int GLOBAL_ID = rdr.GetOrdinal("global_id");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    ExternalWS.Entity.Global.Person person = null;
                    while (rdr.Read())
                    {
                        person = new ExternalWS.Entity.Global.Person();
                        person.Names = rdr[NAMES] is DBNull ? string.Empty : (string)rdr[NAMES];
                        person.FathersFamilyName = rdr[FATHERS_FAMILY_NAME] is DBNull ? string.Empty : (string)rdr[FATHERS_FAMILY_NAME];
                        person.MothersFamilyName = rdr[MOTHERS_FAMILY_NAME] is DBNull ? string.Empty : (string)rdr[MOTHERS_FAMILY_NAME];
                        person.Email = rdr[EMAIL] is DBNull ? string.Empty : (string)rdr[EMAIL];
                        person.BirthDate = rdr[BIRTH_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[BIRTH_DATE];
                        person.GlobalId = rdr[GLOBAL_ID] is DBNull ? string.Empty : (string)rdr[GLOBAL_ID];
                        person.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        person.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        person.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        personList.Add(person);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return personList;
        }

        [Flags]
        enum PersonFilterType : long
        {
            None = 1,
            Names = 4,
            FathersFamilyName = 8,
            MothersFamilyName = 16,
            Email = 32,
            BirthDate = 64,
            GlobalId = 128,
        }

        PersonFilterType GetPersonFilterType(FilterPerson filterPerson)
        {
            if (filterPerson == null) return PersonFilterType.None;
            PersonFilterType filterType = new PersonFilterType();
            if (filterPerson.Names != null && filterPerson.Names.Length > 0) filterType |= PersonFilterType.Names;
            if (filterPerson.FathersFamilyName != null && filterPerson.FathersFamilyName.Length > 0) filterType |= PersonFilterType.FathersFamilyName;
            if (filterPerson.MothersFamilyName != null && filterPerson.MothersFamilyName.Length > 0) filterType |= PersonFilterType.MothersFamilyName;
            if (filterPerson.Email != null && filterPerson.Email.Length > 0) filterType |= PersonFilterType.Email;
            if (filterPerson.BirthDate != DateTime.MinValue) filterType |= PersonFilterType.BirthDate;
            if (filterPerson.GlobalId != null && filterPerson.GlobalId.Length > 0) filterType |= PersonFilterType.GlobalId;
            return filterType;
        }

        #endregion

        #region GetPerson()
        public List<ExternalWS.Entity.Global.Person> GetPerson()
        {

            List<ExternalWS.Entity.Global.Person> personList = null;
            personList = new List<ExternalWS.Entity.Global.Person>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_PERSON_ALL);
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int NAMES = rdr.GetOrdinal("names");
                    int FATHERS_FAMILY_NAME = rdr.GetOrdinal("fathers_family_name");
                    int MOTHERS_FAMILY_NAME = rdr.GetOrdinal("mothers_family_name");
                    int EMAIL = rdr.GetOrdinal("email");
                    int BIRTH_DATE = rdr.GetOrdinal("birth_date");
                    int GLOBAL_ID = rdr.GetOrdinal("global_id");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");

                    ExternalWS.Entity.Global.Person person = null;
                    while (rdr.Read())
                    {
                        person = new ExternalWS.Entity.Global.Person();
                        person.Names = rdr[NAMES] is DBNull ? string.Empty : (string)rdr[NAMES];
                        person.FathersFamilyName = rdr[FATHERS_FAMILY_NAME] is DBNull ? string.Empty : (string)rdr[FATHERS_FAMILY_NAME];
                        person.MothersFamilyName = rdr[MOTHERS_FAMILY_NAME] is DBNull ? string.Empty : (string)rdr[MOTHERS_FAMILY_NAME];
                        person.Email = rdr[EMAIL] is DBNull ? string.Empty : (string)rdr[EMAIL];
                        person.BirthDate = rdr[BIRTH_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[BIRTH_DATE];
                        person.GlobalId = rdr[GLOBAL_ID] is DBNull ? string.Empty : (string)rdr[GLOBAL_ID];
                        person.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];

                        person.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        person.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        personList.Add(person);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return personList;
        }
        #endregion

        #region GetPerson(int id)
        public ExternalWS.Entity.Global.Person GetPerson(int id)
        {
            List<ExternalWS.Entity.Global.Person> personList = new List<ExternalWS.Entity.Global.Person>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_PERSON_BY_ID);
            cmd.AddInParameter("@id", DbType.Int32, id);
            ExternalWS.Entity.Global.Person person = null;
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int NAMES = rdr.GetOrdinal("names");
                    int FATHERS_FAMILY_NAME = rdr.GetOrdinal("fathers_family_name");
                    int MOTHERS_FAMILY_NAME = rdr.GetOrdinal("mothers_family_name");
                    int EMAIL = rdr.GetOrdinal("email");
                    int BIRTH_DATE = rdr.GetOrdinal("birth_date");
                    int GLOBAL_ID = rdr.GetOrdinal("global_id");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    while (rdr.Read())
                    {
                        person = new ExternalWS.Entity.Global.Person();
                        person.Names = rdr[NAMES] is DBNull ? string.Empty : (string)rdr[NAMES];
                        person.FathersFamilyName = rdr[FATHERS_FAMILY_NAME] is DBNull ? string.Empty : (string)rdr[FATHERS_FAMILY_NAME];
                        person.MothersFamilyName = rdr[MOTHERS_FAMILY_NAME] is DBNull ? string.Empty : (string)rdr[MOTHERS_FAMILY_NAME];
                        person.Email = rdr[EMAIL] is DBNull ? string.Empty : (string)rdr[EMAIL];
                        person.BirthDate = rdr[BIRTH_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[BIRTH_DATE];
                        person.GlobalId = rdr[GLOBAL_ID] is DBNull ? string.Empty : (string)rdr[GLOBAL_ID];
                        person.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        person.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        person.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);

                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return person;
        }
        #endregion

        #region Constants
        private const string SP_INSERT_PRODUCTO = "Global.insert_producto";
        private const string SP_UPDATE_PRODUCTO = "Global.update_producto";
        private const string SP_DELETE_PRODUCTO = "Global.delete_producto";
        private const string GET_PRODUCTO_ALL = "Global.get_producto_all";
        private const string GET_PRODUCTO_BY_ID = "Global.get_producto_by_id";
        private const string GET_PRODUCTO_BY_DYNAMIC_PARAMETERS = "Global.get_producto_by_dynamic_parameters";
        private const string GET_PRODUCTO_BY_ID_PRODUCTO = "Global.get_producto_by_id_producto";
        private const string GET_PRODUCTO_BY_DESCRIPCION = "Global.get_producto_by_descripcion";
        #endregion

        #region InsertProducto(ExternalWS.Entity.Global.Producto producto)
        public void InsertProducto(ExternalWS.Entity.Global.Producto producto)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_INSERT_PRODUCTO);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);


            cmd.AddInParameter("id_producto", DbType.Int32, producto.idProducto == 0 ? null : (object)producto.idProducto);
            cmd.AddInParameter("descripcion", DbType.String, producto.descripcion); cmd.AddOutParameter(PARAM_OID, DbType.Int32, 0);
            cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                producto.Id = (int)cmd.GetParameterValue(PARAM_OID);
                producto.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region UpdateProducto(ExternalWS.Entity.Global.Producto producto)
        public void UpdateProducto(ExternalWS.Entity.Global.Producto producto)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_UPDATE_PRODUCTO);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);

            cmd.AddInParameter(PARAM_ID, DbType.Int32, producto.Id);
            cmd.AddInParameter("id_producto", DbType.Int32, producto.idProducto == 0 ? null : (object)producto.idProducto);
            cmd.AddInParameter("descripcion", DbType.String, producto.descripcion);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(producto.TimeStamp as string));
            //cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                //producto.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region DeleteProducto(ExternalWS.Entity.Global.Producto producto)
        public void DeleteProducto(ExternalWS.Entity.Global.Producto producto)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_DELETE_PRODUCTO);
            cmd.AddInParameter(PARAM_ID, DbType.Int32, producto.Id);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);
            //cmd.AddInParameter(PARAM_IS_DELETED, DbType.Boolean, 1);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(producto.TimeStamp as string));
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                mt.AcceptTransaction();
            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region GetProducto(ExternalWS.Entity.Global.FilterProducto filterProducto)
        public List<ExternalWS.Entity.Global.Producto> GetProducto(ExternalWS.Entity.Global.FilterProducto filterProducto)
        {
            List<ExternalWS.Entity.Global.Producto> productoList = new List<ExternalWS.Entity.Global.Producto>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = null;
            ProductoFilterType filterType = GetProductoFilterType(filterProducto);
            switch (filterType)
            {
                case ProductoFilterType.None:
                    throw new Exception("Empty Filter");
                case ProductoFilterType.idProducto:
                    cmd = mt.CreateCommand(GET_PRODUCTO_BY_ID_PRODUCTO);
                    cmd.AddInParameter("id_producto", DbType.Int32, filterProducto.idProducto);
                    break;

                case ProductoFilterType.descripcion:
                    cmd = mt.CreateCommand(GET_PRODUCTO_BY_DESCRIPCION);
                    cmd.AddInParameter("descripcion", DbType.String, filterProducto.descripcion);
                    break;

                default:
                    cmd = mt.CreateCommand(GET_PRODUCTO_BY_DYNAMIC_PARAMETERS);
                    cmd.AddInParameter("@id", DbType.Int32, 0);

                    cmd.AddInParameter("id_producto", DbType.Int32, filterProducto.idProducto);
                    cmd.AddInParameter("descripcion", DbType.String, filterProducto.descripcion == "" ? null : filterProducto.descripcion); break;
            }

            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int ID_PRODUCTO = rdr.GetOrdinal("id_producto");
                    int DESCRIPCION = rdr.GetOrdinal("descripcion");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    ExternalWS.Entity.Global.Producto producto = null;
                    while (rdr.Read())
                    {
                        producto = new ExternalWS.Entity.Global.Producto();
                        producto.idProducto = rdr[ID_PRODUCTO] is DBNull ? 0 : (int)rdr[ID_PRODUCTO];
                        producto.descripcion = rdr[DESCRIPCION] is DBNull ? string.Empty : (string)rdr[DESCRIPCION];
                        producto.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        producto.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        producto.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        productoList.Add(producto);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return productoList;
        }

        [Flags]
        enum ProductoFilterType : long
        {
            None = 1,
            idProducto = 4,
            descripcion = 8,
        }

        ProductoFilterType GetProductoFilterType(FilterProducto filterProducto)
        {
            if (filterProducto == null) return ProductoFilterType.None;
            ProductoFilterType filterType = new ProductoFilterType();
            if (filterProducto.idProducto > 0) filterType |= ProductoFilterType.idProducto;
            if (filterProducto.descripcion != null && filterProducto.descripcion.Length > 0) filterType |= ProductoFilterType.descripcion;
            return filterType;
        }

        #endregion

        #region GetProducto()
        public List<ExternalWS.Entity.Global.Producto> GetProducto()
        {

            List<ExternalWS.Entity.Global.Producto> productoList = null;
            productoList = new List<ExternalWS.Entity.Global.Producto>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_PRODUCTO_ALL);
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int ID_PRODUCTO = rdr.GetOrdinal("id_producto");
                    int DESCRIPCION = rdr.GetOrdinal("descripcion");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");

                    ExternalWS.Entity.Global.Producto producto = null;
                    while (rdr.Read())
                    {
                        producto = new ExternalWS.Entity.Global.Producto();
                        producto.idProducto = rdr[ID_PRODUCTO] is DBNull ? 0 : (int)rdr[ID_PRODUCTO];
                        producto.descripcion = rdr[DESCRIPCION] is DBNull ? string.Empty : (string)rdr[DESCRIPCION];
                        producto.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];

                        producto.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        producto.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        productoList.Add(producto);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return productoList;
        }
        #endregion

        #region GetProducto(int id)
        public ExternalWS.Entity.Global.Producto GetProducto(int id)
        {
            List<ExternalWS.Entity.Global.Producto> productoList = new List<ExternalWS.Entity.Global.Producto>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_PRODUCTO_BY_ID);
            cmd.AddInParameter("@id", DbType.Int32, id);
            ExternalWS.Entity.Global.Producto producto = null;
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int ID_PRODUCTO = rdr.GetOrdinal("id_producto");
                    int DESCRIPCION = rdr.GetOrdinal("descripcion");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    while (rdr.Read())
                    {
                        producto = new ExternalWS.Entity.Global.Producto();
                        producto.idProducto = rdr[ID_PRODUCTO] is DBNull ? 0 : (int)rdr[ID_PRODUCTO];
                        producto.descripcion = rdr[DESCRIPCION] is DBNull ? string.Empty : (string)rdr[DESCRIPCION];
                        producto.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        producto.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        producto.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);

                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return producto;
        }
        #endregion

        #region Constants
        private const string SP_INSERT_PROVEEDOR = "Global.insert_proveedor";
        private const string SP_UPDATE_PROVEEDOR = "Global.update_proveedor";
        private const string SP_DELETE_PROVEEDOR = "Global.delete_proveedor";
        private const string GET_PROVEEDOR_ALL = "Global.get_proveedor_all";
        private const string GET_PROVEEDOR_BY_ID = "Global.get_proveedor_by_id";
        private const string GET_PROVEEDOR_BY_DYNAMIC_PARAMETERS = "Global.get_proveedor_by_dynamic_parameters";
        private const string GET_PROVEEDOR_BY_RUN_PROVEEDOR = "Global.get_proveedor_by_run_proveedor";
        private const string GET_PROVEEDOR_BY_NOMBRE_PROVEEDOR = "Global.get_proveedor_by_nombre_proveedor";
        private const string GET_PROVEEDOR_BY_DIRECCION_PROVEEDOR = "Global.get_proveedor_by_direccion_proveedor";
        private const string GET_PROVEEDOR_BY_FONO_PROVEEDOR = "Global.get_proveedor_by_fono_proveedor";
        private const string GET_PROVEEDOR_BY_EMAIL_PROVEEDOR = "Global.get_proveedor_by_email_proveedor";
        #endregion

        #region InsertProveedor(ExternalWS.Entity.Global.Proveedor proveedor)
        public void InsertProveedor(ExternalWS.Entity.Global.Proveedor proveedor)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_INSERT_PROVEEDOR);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);


            cmd.AddInParameter("run_proveedor", DbType.String, proveedor.runProveedor);
            cmd.AddInParameter("nombre_proveedor", DbType.String, proveedor.nombreProveedor);
            cmd.AddInParameter("direccion_proveedor", DbType.String, proveedor.direcionProveedor);
            cmd.AddInParameter("fono_proveedor", DbType.Int32, proveedor.fonoProveedor == 0 ? null : (object)proveedor.fonoProveedor);
            cmd.AddInParameter("email_proveedor", DbType.String, proveedor.emailProveedor); cmd.AddOutParameter(PARAM_OID, DbType.Int32, 0);
            cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                proveedor.Id = (int)cmd.GetParameterValue(PARAM_OID);
                proveedor.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region UpdateProveedor(ExternalWS.Entity.Global.Proveedor proveedor)
        public void UpdateProveedor(ExternalWS.Entity.Global.Proveedor proveedor)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_UPDATE_PROVEEDOR);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);

            cmd.AddInParameter(PARAM_ID, DbType.Int32, proveedor.Id);
            cmd.AddInParameter("run_proveedor", DbType.String, proveedor.runProveedor);
            cmd.AddInParameter("nombre_proveedor", DbType.String, proveedor.nombreProveedor);
            cmd.AddInParameter("direccion_proveedor", DbType.String, proveedor.direcionProveedor);
            cmd.AddInParameter("fono_proveedor", DbType.Int32, proveedor.fonoProveedor == 0 ? null : (object)proveedor.fonoProveedor);
            cmd.AddInParameter("email_proveedor", DbType.String, proveedor.emailProveedor);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(proveedor.TimeStamp as string));
            //cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                //proveedor.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region DeleteProveedor(ExternalWS.Entity.Global.Proveedor proveedor)
        public void DeleteProveedor(ExternalWS.Entity.Global.Proveedor proveedor)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_DELETE_PROVEEDOR);
            cmd.AddInParameter(PARAM_ID, DbType.Int32, proveedor.Id);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);
            //cmd.AddInParameter(PARAM_IS_DELETED, DbType.Boolean, 1);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(proveedor.TimeStamp as string));
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                mt.AcceptTransaction();
            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region GetProveedor(ExternalWS.Entity.Global.FilterProveedor filterProveedor)
        public List<ExternalWS.Entity.Global.Proveedor> GetProveedor(ExternalWS.Entity.Global.FilterProveedor filterProveedor)
        {
            List<ExternalWS.Entity.Global.Proveedor> proveedorList = new List<ExternalWS.Entity.Global.Proveedor>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = null;
            ProveedorFilterType filterType = GetProveedorFilterType(filterProveedor);
            switch (filterType)
            {
                case ProveedorFilterType.None:
                    throw new Exception("Empty Filter");
                case ProveedorFilterType.runProveedor:
                    cmd = mt.CreateCommand(GET_PROVEEDOR_BY_RUN_PROVEEDOR);
                    cmd.AddInParameter("run_proveedor", DbType.String, filterProveedor.runProveedor);
                    break;

                case ProveedorFilterType.nombreProveedor:
                    cmd = mt.CreateCommand(GET_PROVEEDOR_BY_NOMBRE_PROVEEDOR);
                    cmd.AddInParameter("nombre_proveedor", DbType.String, filterProveedor.nombreProveedor);
                    break;

                case ProveedorFilterType.direcionProveedor:
                    cmd = mt.CreateCommand(GET_PROVEEDOR_BY_DIRECCION_PROVEEDOR);
                    cmd.AddInParameter("direccion_proveedor", DbType.String, filterProveedor.direcionProveedor);
                    break;

                case ProveedorFilterType.fonoProveedor:
                    cmd = mt.CreateCommand(GET_PROVEEDOR_BY_FONO_PROVEEDOR);
                    cmd.AddInParameter("fono_proveedor", DbType.Int32, filterProveedor.fonoProveedor);
                    break;

                case ProveedorFilterType.emailProveedor:
                    cmd = mt.CreateCommand(GET_PROVEEDOR_BY_EMAIL_PROVEEDOR);
                    cmd.AddInParameter("email_proveedor", DbType.String, filterProveedor.emailProveedor);
                    break;

                default:
                    cmd = mt.CreateCommand(GET_PROVEEDOR_BY_DYNAMIC_PARAMETERS);
                    cmd.AddInParameter("@id", DbType.Int32, 0);

                    cmd.AddInParameter("run_proveedor", DbType.String, filterProveedor.runProveedor == "" ? null : filterProveedor.runProveedor);
                    cmd.AddInParameter("nombre_proveedor", DbType.String, filterProveedor.nombreProveedor == "" ? null : filterProveedor.nombreProveedor);
                    cmd.AddInParameter("direccion_proveedor", DbType.String, filterProveedor.direcionProveedor == "" ? null : filterProveedor.direcionProveedor);
                    cmd.AddInParameter("fono_proveedor", DbType.Int32, filterProveedor.fonoProveedor);
                    cmd.AddInParameter("email_proveedor", DbType.String, filterProveedor.emailProveedor == "" ? null : filterProveedor.emailProveedor); break;
            }

            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int RUN_PROVEEDOR = rdr.GetOrdinal("run_proveedor");
                    int NOMBRE_PROVEEDOR = rdr.GetOrdinal("nombre_proveedor");
                    int DIRECCION_PROVEEDOR = rdr.GetOrdinal("direccion_proveedor");
                    int FONO_PROVEEDOR = rdr.GetOrdinal("fono_proveedor");
                    int EMAIL_PROVEEDOR = rdr.GetOrdinal("email_proveedor");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    ExternalWS.Entity.Global.Proveedor proveedor = null;
                    while (rdr.Read())
                    {
                        proveedor = new ExternalWS.Entity.Global.Proveedor();
                        proveedor.runProveedor = rdr[RUN_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[RUN_PROVEEDOR];
                        proveedor.nombreProveedor = rdr[NOMBRE_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[NOMBRE_PROVEEDOR];
                        proveedor.direcionProveedor = rdr[DIRECCION_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[DIRECCION_PROVEEDOR];
                        proveedor.fonoProveedor = rdr[FONO_PROVEEDOR] is DBNull ? 0 : (int)rdr[FONO_PROVEEDOR];
                        proveedor.emailProveedor = rdr[EMAIL_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[EMAIL_PROVEEDOR];
                        proveedor.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        proveedor.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        proveedor.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        proveedorList.Add(proveedor);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return proveedorList;
        }

        [Flags]
        enum ProveedorFilterType : long
        {
            None = 1,
            runProveedor = 4,
            nombreProveedor = 8,
            direcionProveedor = 16,
            fonoProveedor = 32,
            emailProveedor = 64,
        }

        ProveedorFilterType GetProveedorFilterType(FilterProveedor filterProveedor)
        {
            if (filterProveedor == null) return ProveedorFilterType.None;
            ProveedorFilterType filterType = new ProveedorFilterType();
            if (filterProveedor.runProveedor != null && filterProveedor.runProveedor.Length > 0) filterType |= ProveedorFilterType.runProveedor;
            if (filterProveedor.nombreProveedor != null && filterProveedor.nombreProveedor.Length > 0) filterType |= ProveedorFilterType.nombreProveedor;
            if (filterProveedor.direcionProveedor != null && filterProveedor.direcionProveedor.Length > 0) filterType |= ProveedorFilterType.direcionProveedor;
            if (filterProveedor.fonoProveedor > 0) filterType |= ProveedorFilterType.fonoProveedor;
            if (filterProveedor.emailProveedor != null && filterProveedor.emailProveedor.Length > 0) filterType |= ProveedorFilterType.emailProveedor;
            return filterType;
        }

        #endregion

        #region GetProveedor()
        public List<ExternalWS.Entity.Global.Proveedor> GetProveedor()
        {

            List<ExternalWS.Entity.Global.Proveedor> proveedorList = null;
            proveedorList = new List<ExternalWS.Entity.Global.Proveedor>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_PROVEEDOR_ALL);
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int RUN_PROVEEDOR = rdr.GetOrdinal("run_proveedor");
                    int NOMBRE_PROVEEDOR = rdr.GetOrdinal("nombre_proveedor");
                    int DIRECCION_PROVEEDOR = rdr.GetOrdinal("direccion_proveedor");
                    int FONO_PROVEEDOR = rdr.GetOrdinal("fono_proveedor");
                    int EMAIL_PROVEEDOR = rdr.GetOrdinal("email_proveedor");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");

                    ExternalWS.Entity.Global.Proveedor proveedor = null;
                    while (rdr.Read())
                    {
                        proveedor = new ExternalWS.Entity.Global.Proveedor();
                        proveedor.runProveedor = rdr[RUN_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[RUN_PROVEEDOR];
                        proveedor.nombreProveedor = rdr[NOMBRE_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[NOMBRE_PROVEEDOR];
                        proveedor.direcionProveedor = rdr[DIRECCION_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[DIRECCION_PROVEEDOR];
                        proveedor.fonoProveedor = rdr[FONO_PROVEEDOR] is DBNull ? 0 : (int)rdr[FONO_PROVEEDOR];
                        proveedor.emailProveedor = rdr[EMAIL_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[EMAIL_PROVEEDOR];
                        proveedor.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];

                        proveedor.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        proveedor.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        proveedorList.Add(proveedor);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return proveedorList;
        }
        #endregion

        #region GetProveedor(int id)
        public ExternalWS.Entity.Global.Proveedor GetProveedor(int id)
        {
            List<ExternalWS.Entity.Global.Proveedor> proveedorList = new List<ExternalWS.Entity.Global.Proveedor>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_PROVEEDOR_BY_ID);
            cmd.AddInParameter("@id", DbType.Int32, id);
            ExternalWS.Entity.Global.Proveedor proveedor = null;
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int RUN_PROVEEDOR = rdr.GetOrdinal("run_proveedor");
                    int NOMBRE_PROVEEDOR = rdr.GetOrdinal("nombre_proveedor");
                    int DIRECCION_PROVEEDOR = rdr.GetOrdinal("direccion_proveedor");
                    int FONO_PROVEEDOR = rdr.GetOrdinal("fono_proveedor");
                    int EMAIL_PROVEEDOR = rdr.GetOrdinal("email_proveedor");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    while (rdr.Read())
                    {
                        proveedor = new ExternalWS.Entity.Global.Proveedor();
                        proveedor.runProveedor = rdr[RUN_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[RUN_PROVEEDOR];
                        proveedor.nombreProveedor = rdr[NOMBRE_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[NOMBRE_PROVEEDOR];
                        proveedor.direcionProveedor = rdr[DIRECCION_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[DIRECCION_PROVEEDOR];
                        proveedor.fonoProveedor = rdr[FONO_PROVEEDOR] is DBNull ? 0 : (int)rdr[FONO_PROVEEDOR];
                        proveedor.emailProveedor = rdr[EMAIL_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[EMAIL_PROVEEDOR];
                        proveedor.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        proveedor.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        proveedor.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);

                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return proveedor;
        }
        #endregion

        #region Constants
        private const string SP_INSERT_CABECERA_ORDEN_COMPRA = "Global.insert_cabecera_orden_compra";
        private const string SP_UPDATE_CABECERA_ORDEN_COMPRA = "Global.update_cabecera_orden_compra";
        private const string SP_DELETE_CABECERA_ORDEN_COMPRA = "Global.delete_cabecera_orden_compra";
        private const string GET_CABECERA_ORDEN_COMPRA_ALL = "Global.get_cabecera_orden_compra_all";
        private const string GET_CABECERA_ORDEN_COMPRA_BY_ID = "Global.get_cabecera_orden_compra_by_id";
        private const string GET_CABECERA_ORDEN_COMPRA_BY_DYNAMIC_PARAMETERS = "Global.get_cabecera_orden_compra_by_dynamic_parameters";
        private const string GET_CABECERA_ORDEN_COMPRA_BY_CODIGO_ORDEN_COMPRA = "Global.get_cabecera_orden_compra_by_codigo_orden_compra";
        private const string GET_CABECERA_ORDEN_COMPRA_BY_RUN_CLIENTE = "Global.get_cabecera_orden_compra_by_run_cliente";
        private const string GET_CABECERA_ORDEN_COMPRA_BY_NOMBRE_CLIENTE = "Global.get_cabecera_orden_compra_by_nombre_cliente";
        private const string GET_CABECERA_ORDEN_COMPRA_BY_DIRECCION_CLIENTE = "Global.get_cabecera_orden_compra_by_direccion_cliente";
        private const string GET_CABECERA_ORDEN_COMPRA_BY_FONO_CLIENTE = "Global.get_cabecera_orden_compra_by_fono_cliente";
        private const string GET_CABECERA_ORDEN_COMPRA_BY_EMAIL_CLIENTE = "Global.get_cabecera_orden_compra_by_email_cliente";
        private const string GET_CABECERA_ORDEN_COMPRA_BY_FECHA = "Global.get_cabecera_orden_compra_by_fecha";
        #endregion

        #region InsertCabeceraOrdenCompra(ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra)
        public void InsertCabeceraOrdenCompra(ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_INSERT_CABECERA_ORDEN_COMPRA);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);


            cmd.AddInParameter("codigo_orden_compra", DbType.String, cabeceraOrdenCompra.codOrdenCompra);
            cmd.AddInParameter("run_cliente", DbType.String, cabeceraOrdenCompra.runCliente);
            cmd.AddInParameter("nombre_cliente", DbType.String, cabeceraOrdenCompra.nombreCliente);
            cmd.AddInParameter("direccion_cliente", DbType.String, cabeceraOrdenCompra.direcionCliente);
            cmd.AddInParameter("fono_cliente", DbType.Int32, cabeceraOrdenCompra.fonoCliente == 0 ? null : (object)cabeceraOrdenCompra.fonoCliente);
            cmd.AddInParameter("email_cliente", DbType.String, cabeceraOrdenCompra.emailCliente);
            cmd.AddInParameter("fecha", DbType.DateTime, cabeceraOrdenCompra.fecha == DateTime.MinValue ? null : (object)cabeceraOrdenCompra.fecha); cmd.AddOutParameter(PARAM_OID, DbType.Int32, 0);
            cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                cabeceraOrdenCompra.Id = (int)cmd.GetParameterValue(PARAM_OID);
                cabeceraOrdenCompra.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region UpdateCabeceraOrdenCompra(ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra)
        public void UpdateCabeceraOrdenCompra(ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_UPDATE_CABECERA_ORDEN_COMPRA);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);

            cmd.AddInParameter(PARAM_ID, DbType.Int32, cabeceraOrdenCompra.Id);
            cmd.AddInParameter("codigo_orden_compra", DbType.String, cabeceraOrdenCompra.codOrdenCompra);
            cmd.AddInParameter("run_cliente", DbType.String, cabeceraOrdenCompra.runCliente);
            cmd.AddInParameter("nombre_cliente", DbType.String, cabeceraOrdenCompra.nombreCliente);
            cmd.AddInParameter("direccion_cliente", DbType.String, cabeceraOrdenCompra.direcionCliente);
            cmd.AddInParameter("fono_cliente", DbType.Int32, cabeceraOrdenCompra.fonoCliente == 0 ? null : (object)cabeceraOrdenCompra.fonoCliente);
            cmd.AddInParameter("email_cliente", DbType.String, cabeceraOrdenCompra.emailCliente);
            cmd.AddInParameter("fecha", DbType.DateTime, cabeceraOrdenCompra.fecha == DateTime.MinValue ? null : (object)cabeceraOrdenCompra.fecha);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(cabeceraOrdenCompra.TimeStamp as string));
            //cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                //cabeceraOrdenCompra.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region DeleteCabeceraOrdenCompra(ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra)
        public void DeleteCabeceraOrdenCompra(ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_DELETE_CABECERA_ORDEN_COMPRA);
            cmd.AddInParameter(PARAM_ID, DbType.Int32, cabeceraOrdenCompra.Id);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);
            //cmd.AddInParameter(PARAM_IS_DELETED, DbType.Boolean, 1);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(cabeceraOrdenCompra.TimeStamp as string));
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                mt.AcceptTransaction();
            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region GetCabeceraOrdenCompra(ExternalWS.Entity.Global.FilterCabeceraOrdenCompra filterCabeceraOrdenCompra)
        public List<ExternalWS.Entity.Global.CabeceraOrdenCompra> GetCabeceraOrdenCompra(ExternalWS.Entity.Global.FilterCabeceraOrdenCompra filterCabeceraOrdenCompra)
        {
            List<ExternalWS.Entity.Global.CabeceraOrdenCompra> cabeceraOrdenCompraList = new List<ExternalWS.Entity.Global.CabeceraOrdenCompra>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = null;
            CabeceraOrdenCompraFilterType filterType = GetCabeceraOrdenCompraFilterType(filterCabeceraOrdenCompra);
            switch (filterType)
            {
                case CabeceraOrdenCompraFilterType.None:
                    throw new Exception("Empty Filter");
                case CabeceraOrdenCompraFilterType.codOrdenCompra:
                    cmd = mt.CreateCommand(GET_CABECERA_ORDEN_COMPRA_BY_CODIGO_ORDEN_COMPRA);
                    cmd.AddInParameter("codigo_orden_compra", DbType.String, filterCabeceraOrdenCompra.codOrdenCompra);
                    break;

                case CabeceraOrdenCompraFilterType.runCliente:
                    cmd = mt.CreateCommand(GET_CABECERA_ORDEN_COMPRA_BY_RUN_CLIENTE);
                    cmd.AddInParameter("run_cliente", DbType.String, filterCabeceraOrdenCompra.runCliente);
                    break;

                case CabeceraOrdenCompraFilterType.nombreCliente:
                    cmd = mt.CreateCommand(GET_CABECERA_ORDEN_COMPRA_BY_NOMBRE_CLIENTE);
                    cmd.AddInParameter("nombre_cliente", DbType.String, filterCabeceraOrdenCompra.nombreCliente);
                    break;

                case CabeceraOrdenCompraFilterType.direcionCliente:
                    cmd = mt.CreateCommand(GET_CABECERA_ORDEN_COMPRA_BY_DIRECCION_CLIENTE);
                    cmd.AddInParameter("direccion_cliente", DbType.String, filterCabeceraOrdenCompra.direcionCliente);
                    break;

                case CabeceraOrdenCompraFilterType.fonoCliente:
                    cmd = mt.CreateCommand(GET_CABECERA_ORDEN_COMPRA_BY_FONO_CLIENTE);
                    cmd.AddInParameter("fono_cliente", DbType.Int32, filterCabeceraOrdenCompra.fonoCliente);
                    break;

                case CabeceraOrdenCompraFilterType.emailCliente:
                    cmd = mt.CreateCommand(GET_CABECERA_ORDEN_COMPRA_BY_EMAIL_CLIENTE);
                    cmd.AddInParameter("email_cliente", DbType.String, filterCabeceraOrdenCompra.emailCliente);
                    break;

                case CabeceraOrdenCompraFilterType.fecha:
                    cmd = mt.CreateCommand(GET_CABECERA_ORDEN_COMPRA_BY_FECHA);
                    cmd.AddInParameter("fecha", DbType.DateTime, filterCabeceraOrdenCompra.fecha == DateTime.MinValue ? null : (object)filterCabeceraOrdenCompra.fecha);
                    break;

                default:
                    cmd = mt.CreateCommand(GET_CABECERA_ORDEN_COMPRA_BY_DYNAMIC_PARAMETERS);
                    cmd.AddInParameter("@id", DbType.Int32, 0);

                    cmd.AddInParameter("codigo_orden_compra", DbType.String, filterCabeceraOrdenCompra.codOrdenCompra == "" ? null : filterCabeceraOrdenCompra.codOrdenCompra);
                    cmd.AddInParameter("run_cliente", DbType.String, filterCabeceraOrdenCompra.runCliente == "" ? null : filterCabeceraOrdenCompra.runCliente);
                    cmd.AddInParameter("nombre_cliente", DbType.String, filterCabeceraOrdenCompra.nombreCliente == "" ? null : filterCabeceraOrdenCompra.nombreCliente);
                    cmd.AddInParameter("direccion_cliente", DbType.String, filterCabeceraOrdenCompra.direcionCliente == "" ? null : filterCabeceraOrdenCompra.direcionCliente);
                    cmd.AddInParameter("fono_cliente", DbType.Int32, filterCabeceraOrdenCompra.fonoCliente);
                    cmd.AddInParameter("email_cliente", DbType.String, filterCabeceraOrdenCompra.emailCliente == "" ? null : filterCabeceraOrdenCompra.emailCliente);
                    cmd.AddInParameter("fecha", DbType.DateTime, filterCabeceraOrdenCompra.fecha == DateTime.MinValue ? null : (object)filterCabeceraOrdenCompra.fecha); break;
            }

            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CODIGO_ORDEN_COMPRA = rdr.GetOrdinal("codigo_orden_compra");
                    int RUN_CLIENTE = rdr.GetOrdinal("run_cliente");
                    int NOMBRE_CLIENTE = rdr.GetOrdinal("nombre_cliente");
                    int DIRECCION_CLIENTE = rdr.GetOrdinal("direccion_cliente");
                    int FONO_CLIENTE = rdr.GetOrdinal("fono_cliente");
                    int EMAIL_CLIENTE = rdr.GetOrdinal("email_cliente");
                    int FECHA = rdr.GetOrdinal("fecha");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra = null;
                    while (rdr.Read())
                    {
                        cabeceraOrdenCompra = new ExternalWS.Entity.Global.CabeceraOrdenCompra();
                        cabeceraOrdenCompra.codOrdenCompra = rdr[CODIGO_ORDEN_COMPRA] is DBNull ? string.Empty : (string)rdr[CODIGO_ORDEN_COMPRA];
                        cabeceraOrdenCompra.runCliente = rdr[RUN_CLIENTE] is DBNull ? string.Empty : (string)rdr[RUN_CLIENTE];
                        cabeceraOrdenCompra.nombreCliente = rdr[NOMBRE_CLIENTE] is DBNull ? string.Empty : (string)rdr[NOMBRE_CLIENTE];
                        cabeceraOrdenCompra.direcionCliente = rdr[DIRECCION_CLIENTE] is DBNull ? string.Empty : (string)rdr[DIRECCION_CLIENTE];
                        cabeceraOrdenCompra.fonoCliente = rdr[FONO_CLIENTE] is DBNull ? 0 : (int)rdr[FONO_CLIENTE];
                        cabeceraOrdenCompra.emailCliente = rdr[EMAIL_CLIENTE] is DBNull ? string.Empty : (string)rdr[EMAIL_CLIENTE];
                        cabeceraOrdenCompra.fecha = rdr[FECHA] is DBNull ? DateTime.MinValue : (DateTime)rdr[FECHA];
                        cabeceraOrdenCompra.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        cabeceraOrdenCompra.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        cabeceraOrdenCompra.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        cabeceraOrdenCompraList.Add(cabeceraOrdenCompra);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return cabeceraOrdenCompraList;
        }

        [Flags]
        enum CabeceraOrdenCompraFilterType : long
        {
            None = 1,
            codOrdenCompra = 4,
            runCliente = 8,
            nombreCliente = 16,
            direcionCliente = 32,
            fonoCliente = 64,
            emailCliente = 128,
            fecha = 256,
        }

        CabeceraOrdenCompraFilterType GetCabeceraOrdenCompraFilterType(FilterCabeceraOrdenCompra filterCabeceraOrdenCompra)
        {
            if (filterCabeceraOrdenCompra == null) return CabeceraOrdenCompraFilterType.None;
            CabeceraOrdenCompraFilterType filterType = new CabeceraOrdenCompraFilterType();
            if (filterCabeceraOrdenCompra.codOrdenCompra != null && filterCabeceraOrdenCompra.codOrdenCompra.Length > 0) filterType |= CabeceraOrdenCompraFilterType.codOrdenCompra;
            if (filterCabeceraOrdenCompra.runCliente != null && filterCabeceraOrdenCompra.runCliente.Length > 0) filterType |= CabeceraOrdenCompraFilterType.runCliente;
            if (filterCabeceraOrdenCompra.nombreCliente != null && filterCabeceraOrdenCompra.nombreCliente.Length > 0) filterType |= CabeceraOrdenCompraFilterType.nombreCliente;
            if (filterCabeceraOrdenCompra.direcionCliente != null && filterCabeceraOrdenCompra.direcionCliente.Length > 0) filterType |= CabeceraOrdenCompraFilterType.direcionCliente;
            if (filterCabeceraOrdenCompra.fonoCliente > 0) filterType |= CabeceraOrdenCompraFilterType.fonoCliente;
            if (filterCabeceraOrdenCompra.emailCliente != null && filterCabeceraOrdenCompra.emailCliente.Length > 0) filterType |= CabeceraOrdenCompraFilterType.emailCliente;
            if (filterCabeceraOrdenCompra.fecha != DateTime.MinValue) filterType |= CabeceraOrdenCompraFilterType.fecha;
            return filterType;
        }

        #endregion

        #region GetCabeceraOrdenCompra()
        public List<ExternalWS.Entity.Global.CabeceraOrdenCompra> GetCabeceraOrdenCompra()
        {

            List<ExternalWS.Entity.Global.CabeceraOrdenCompra> cabeceraOrdenCompraList = null;
            cabeceraOrdenCompraList = new List<ExternalWS.Entity.Global.CabeceraOrdenCompra>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_CABECERA_ORDEN_COMPRA_ALL);
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CODIGO_ORDEN_COMPRA = rdr.GetOrdinal("codigo_orden_compra");
                    int RUN_CLIENTE = rdr.GetOrdinal("run_cliente");
                    int NOMBRE_CLIENTE = rdr.GetOrdinal("nombre_cliente");
                    int DIRECCION_CLIENTE = rdr.GetOrdinal("direccion_cliente");
                    int FONO_CLIENTE = rdr.GetOrdinal("fono_cliente");
                    int EMAIL_CLIENTE = rdr.GetOrdinal("email_cliente");
                    int FECHA = rdr.GetOrdinal("fecha");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");

                    ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra = null;
                    while (rdr.Read())
                    {
                        cabeceraOrdenCompra = new ExternalWS.Entity.Global.CabeceraOrdenCompra();
                        cabeceraOrdenCompra.codOrdenCompra = rdr[CODIGO_ORDEN_COMPRA] is DBNull ? string.Empty : (string)rdr[CODIGO_ORDEN_COMPRA];
                        cabeceraOrdenCompra.runCliente = rdr[RUN_CLIENTE] is DBNull ? string.Empty : (string)rdr[RUN_CLIENTE];
                        cabeceraOrdenCompra.nombreCliente = rdr[NOMBRE_CLIENTE] is DBNull ? string.Empty : (string)rdr[NOMBRE_CLIENTE];
                        cabeceraOrdenCompra.direcionCliente = rdr[DIRECCION_CLIENTE] is DBNull ? string.Empty : (string)rdr[DIRECCION_CLIENTE];
                        cabeceraOrdenCompra.fonoCliente = rdr[FONO_CLIENTE] is DBNull ? 0 : (int)rdr[FONO_CLIENTE];
                        cabeceraOrdenCompra.emailCliente = rdr[EMAIL_CLIENTE] is DBNull ? string.Empty : (string)rdr[EMAIL_CLIENTE];
                        cabeceraOrdenCompra.fecha = rdr[FECHA] is DBNull ? DateTime.MinValue : (DateTime)rdr[FECHA];
                        cabeceraOrdenCompra.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];

                        cabeceraOrdenCompra.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        cabeceraOrdenCompra.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        cabeceraOrdenCompraList.Add(cabeceraOrdenCompra);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return cabeceraOrdenCompraList;
        }
        #endregion

        #region GetCabeceraOrdenCompra(int id)
        public ExternalWS.Entity.Global.CabeceraOrdenCompra GetCabeceraOrdenCompra(int id)
        {
            List<ExternalWS.Entity.Global.CabeceraOrdenCompra> cabeceraOrdenCompraList = new List<ExternalWS.Entity.Global.CabeceraOrdenCompra>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_CABECERA_ORDEN_COMPRA_BY_ID);
            cmd.AddInParameter("@id", DbType.Int32, id);
            ExternalWS.Entity.Global.CabeceraOrdenCompra cabeceraOrdenCompra = null;
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CODIGO_ORDEN_COMPRA = rdr.GetOrdinal("codigo_orden_compra");
                    int RUN_CLIENTE = rdr.GetOrdinal("run_cliente");
                    int NOMBRE_CLIENTE = rdr.GetOrdinal("nombre_cliente");
                    int DIRECCION_CLIENTE = rdr.GetOrdinal("direccion_cliente");
                    int FONO_CLIENTE = rdr.GetOrdinal("fono_cliente");
                    int EMAIL_CLIENTE = rdr.GetOrdinal("email_cliente");
                    int FECHA = rdr.GetOrdinal("fecha");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    while (rdr.Read())
                    {
                        cabeceraOrdenCompra = new ExternalWS.Entity.Global.CabeceraOrdenCompra();
                        cabeceraOrdenCompra.codOrdenCompra = rdr[CODIGO_ORDEN_COMPRA] is DBNull ? string.Empty : (string)rdr[CODIGO_ORDEN_COMPRA];
                        cabeceraOrdenCompra.runCliente = rdr[RUN_CLIENTE] is DBNull ? string.Empty : (string)rdr[RUN_CLIENTE];
                        cabeceraOrdenCompra.nombreCliente = rdr[NOMBRE_CLIENTE] is DBNull ? string.Empty : (string)rdr[NOMBRE_CLIENTE];
                        cabeceraOrdenCompra.direcionCliente = rdr[DIRECCION_CLIENTE] is DBNull ? string.Empty : (string)rdr[DIRECCION_CLIENTE];
                        cabeceraOrdenCompra.fonoCliente = rdr[FONO_CLIENTE] is DBNull ? 0 : (int)rdr[FONO_CLIENTE];
                        cabeceraOrdenCompra.emailCliente = rdr[EMAIL_CLIENTE] is DBNull ? string.Empty : (string)rdr[EMAIL_CLIENTE];
                        cabeceraOrdenCompra.fecha = rdr[FECHA] is DBNull ? DateTime.MinValue : (DateTime)rdr[FECHA];
                        cabeceraOrdenCompra.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        cabeceraOrdenCompra.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        cabeceraOrdenCompra.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);

                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return cabeceraOrdenCompra;
        }
        #endregion

        #region Constants
        private const string SP_INSERT_DETALLE_ORDEN_COMPRA = "Global.insert_detalle_orden_compra";
        private const string SP_UPDATE_DETALLE_ORDEN_COMPRA = "Global.update_detalle_orden_compra";
        private const string SP_DELETE_DETALLE_ORDEN_COMPRA = "Global.delete_detalle_orden_compra";
        private const string GET_DETALLE_ORDEN_COMPRA_ALL = "Global.get_detalle_orden_compra_all";
        private const string GET_DETALLE_ORDEN_COMPRA_BY_ID = "Global.get_detalle_orden_compra_by_id";
        private const string GET_DETALLE_ORDEN_COMPRA_BY_DYNAMIC_PARAMETERS = "Global.get_detalle_orden_compra_by_dynamic_parameters";
        private const string GET_DETALLE_ORDEN_COMPRA_BY_CANTIDAD = "Global.get_detalle_orden_compra_by_cantidad";
        private const string GET_DETALLE_ORDEN_COMPRA_BY_PRECIO_UNITARIO = "Global.get_detalle_orden_compra_by_precio_unitario";
        private const string GET_DETALLE_ORDEN_COMPRA_BY_ID_PRODUCTO = "Global.get_detalle_orden_compra_by_id_producto";
        private const string GET_DETALLE_ORDEN_COMPRA_BY_CODIGO_ORDEN_COMPRA = "Global.get_detalle_orden_compra_by_codigo_orden_compra";
        #endregion

        #region InsertDetalleOrdenCompra(ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra)
        public void InsertDetalleOrdenCompra(ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_INSERT_DETALLE_ORDEN_COMPRA);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);


            cmd.AddInParameter("cantidad", DbType.Int32, detalleOrdenCompra.cantidad == 0 ? null : (object)detalleOrdenCompra.cantidad);
            cmd.AddInParameter("precio_unitario", DbType.Int32, detalleOrdenCompra.precioUnitario == 0 ? null : (object)detalleOrdenCompra.precioUnitario);
            cmd.AddInParameter("id_producto", DbType.Int32, detalleOrdenCompra.idProducto == 0 ? null : (object)detalleOrdenCompra.idProducto);
            cmd.AddInParameter("codigo_orden_compra", DbType.String, detalleOrdenCompra.codOrdenCompra); cmd.AddOutParameter(PARAM_OID, DbType.Int32, 0);
            cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                detalleOrdenCompra.Id = (int)cmd.GetParameterValue(PARAM_OID);
                detalleOrdenCompra.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region UpdateDetalleOrdenCompra(ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra)
        public void UpdateDetalleOrdenCompra(ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_UPDATE_DETALLE_ORDEN_COMPRA);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);

            cmd.AddInParameter(PARAM_ID, DbType.Int32, detalleOrdenCompra.Id);
            cmd.AddInParameter("cantidad", DbType.Int32, detalleOrdenCompra.cantidad == 0 ? null : (object)detalleOrdenCompra.cantidad);
            cmd.AddInParameter("precio_unitario", DbType.Int32, detalleOrdenCompra.precioUnitario == 0 ? null : (object)detalleOrdenCompra.precioUnitario);
            cmd.AddInParameter("id_producto", DbType.Int32, detalleOrdenCompra.idProducto == 0 ? null : (object)detalleOrdenCompra.idProducto);
            cmd.AddInParameter("codigo_orden_compra", DbType.String, detalleOrdenCompra.codOrdenCompra);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(detalleOrdenCompra.TimeStamp as string));
            //cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                //detalleOrdenCompra.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region DeleteDetalleOrdenCompra(ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra)
        public void DeleteDetalleOrdenCompra(ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_DELETE_DETALLE_ORDEN_COMPRA);
            cmd.AddInParameter(PARAM_ID, DbType.Int32, detalleOrdenCompra.Id);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);
            //cmd.AddInParameter(PARAM_IS_DELETED, DbType.Boolean, 1);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(detalleOrdenCompra.TimeStamp as string));
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                mt.AcceptTransaction();
            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region GetDetalleOrdenCompra(ExternalWS.Entity.Global.FilterDetalleOrdenCompra filterDetalleOrdenCompra)
        public List<ExternalWS.Entity.Global.DetalleOrdenCompra> GetDetalleOrdenCompra(ExternalWS.Entity.Global.FilterDetalleOrdenCompra filterDetalleOrdenCompra)
        {
            List<ExternalWS.Entity.Global.DetalleOrdenCompra> detalleOrdenCompraList = new List<ExternalWS.Entity.Global.DetalleOrdenCompra>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = null;
            DetalleOrdenCompraFilterType filterType = GetDetalleOrdenCompraFilterType(filterDetalleOrdenCompra);
            switch (filterType)
            {
                case DetalleOrdenCompraFilterType.None:
                    throw new Exception("Empty Filter");
                case DetalleOrdenCompraFilterType.cantidad:
                    cmd = mt.CreateCommand(GET_DETALLE_ORDEN_COMPRA_BY_CANTIDAD);
                    cmd.AddInParameter("cantidad", DbType.Int32, filterDetalleOrdenCompra.cantidad);
                    break;

                case DetalleOrdenCompraFilterType.precioUnitario:
                    cmd = mt.CreateCommand(GET_DETALLE_ORDEN_COMPRA_BY_PRECIO_UNITARIO);
                    cmd.AddInParameter("precio_unitario", DbType.Int32, filterDetalleOrdenCompra.precioUnitario);
                    break;

                case DetalleOrdenCompraFilterType.idProducto:
                    cmd = mt.CreateCommand(GET_DETALLE_ORDEN_COMPRA_BY_ID_PRODUCTO);
                    cmd.AddInParameter("id_producto", DbType.Int32, filterDetalleOrdenCompra.idProducto);
                    break;

                case DetalleOrdenCompraFilterType.codOrdenCompra:
                    cmd = mt.CreateCommand(GET_DETALLE_ORDEN_COMPRA_BY_CODIGO_ORDEN_COMPRA);
                    cmd.AddInParameter("codigo_orden_compra", DbType.String, filterDetalleOrdenCompra.codOrdenCompra);
                    break;

                default:
                    cmd = mt.CreateCommand(GET_DETALLE_ORDEN_COMPRA_BY_DYNAMIC_PARAMETERS);
                    cmd.AddInParameter("@id", DbType.Int32, 0);

                    cmd.AddInParameter("cantidad", DbType.Int32, filterDetalleOrdenCompra.cantidad);
                    cmd.AddInParameter("precio_unitario", DbType.Int32, filterDetalleOrdenCompra.precioUnitario);
                    cmd.AddInParameter("id_producto", DbType.Int32, filterDetalleOrdenCompra.idProducto);
                    cmd.AddInParameter("codigo_orden_compra", DbType.String, filterDetalleOrdenCompra.codOrdenCompra == "" ? null : filterDetalleOrdenCompra.codOrdenCompra); break;
            }

            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CANTIDAD = rdr.GetOrdinal("cantidad");
                    int PRECIO_UNITARIO = rdr.GetOrdinal("precio_unitario");
                    int ID_PRODUCTO = rdr.GetOrdinal("id_producto");
                    int CODIGO_ORDEN_COMPRA = rdr.GetOrdinal("codigo_orden_compra");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra = null;
                    while (rdr.Read())
                    {
                        detalleOrdenCompra = new ExternalWS.Entity.Global.DetalleOrdenCompra();
                        detalleOrdenCompra.cantidad = rdr[CANTIDAD] is DBNull ? 0 : (int)rdr[CANTIDAD];
                        detalleOrdenCompra.precioUnitario = rdr[PRECIO_UNITARIO] is DBNull ? 0 : (int)rdr[PRECIO_UNITARIO];
                        detalleOrdenCompra.idProducto = rdr[ID_PRODUCTO] is DBNull ? 0 : (int)rdr[ID_PRODUCTO];
                        detalleOrdenCompra.codOrdenCompra = rdr[CODIGO_ORDEN_COMPRA] is DBNull ? string.Empty : (string)rdr[CODIGO_ORDEN_COMPRA];
                        detalleOrdenCompra.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        detalleOrdenCompra.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        detalleOrdenCompra.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        detalleOrdenCompraList.Add(detalleOrdenCompra);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return detalleOrdenCompraList;
        }

        [Flags]
        enum DetalleOrdenCompraFilterType : long
        {
            None = 1,
            cantidad = 4,
            precioUnitario = 8,
            idProducto = 16,
            codOrdenCompra = 32,
        }

        DetalleOrdenCompraFilterType GetDetalleOrdenCompraFilterType(FilterDetalleOrdenCompra filterDetalleOrdenCompra)
        {
            if (filterDetalleOrdenCompra == null) return DetalleOrdenCompraFilterType.None;
            DetalleOrdenCompraFilterType filterType = new DetalleOrdenCompraFilterType();
            if (filterDetalleOrdenCompra.cantidad > 0) filterType |= DetalleOrdenCompraFilterType.cantidad;
            if (filterDetalleOrdenCompra.precioUnitario > 0) filterType |= DetalleOrdenCompraFilterType.precioUnitario;
            if (filterDetalleOrdenCompra.idProducto > 0) filterType |= DetalleOrdenCompraFilterType.idProducto;
            if (filterDetalleOrdenCompra.codOrdenCompra != null && filterDetalleOrdenCompra.codOrdenCompra.Length > 0) filterType |= DetalleOrdenCompraFilterType.codOrdenCompra;
            return filterType;
        }

        #endregion

        #region GetDetalleOrdenCompra()
        public List<ExternalWS.Entity.Global.DetalleOrdenCompra> GetDetalleOrdenCompra()
        {

            List<ExternalWS.Entity.Global.DetalleOrdenCompra> detalleOrdenCompraList = null;
            detalleOrdenCompraList = new List<ExternalWS.Entity.Global.DetalleOrdenCompra>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_DETALLE_ORDEN_COMPRA_ALL);
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CANTIDAD = rdr.GetOrdinal("cantidad");
                    int PRECIO_UNITARIO = rdr.GetOrdinal("precio_unitario");
                    int ID_PRODUCTO = rdr.GetOrdinal("id_producto");
                    int CODIGO_ORDEN_COMPRA = rdr.GetOrdinal("codigo_orden_compra");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");

                    ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra = null;
                    while (rdr.Read())
                    {
                        detalleOrdenCompra = new ExternalWS.Entity.Global.DetalleOrdenCompra();
                        detalleOrdenCompra.cantidad = rdr[CANTIDAD] is DBNull ? 0 : (int)rdr[CANTIDAD];
                        detalleOrdenCompra.precioUnitario = rdr[PRECIO_UNITARIO] is DBNull ? 0 : (int)rdr[PRECIO_UNITARIO];
                        detalleOrdenCompra.idProducto = rdr[ID_PRODUCTO] is DBNull ? 0 : (int)rdr[ID_PRODUCTO];
                        detalleOrdenCompra.codOrdenCompra = rdr[CODIGO_ORDEN_COMPRA] is DBNull ? string.Empty : (string)rdr[CODIGO_ORDEN_COMPRA];
                        detalleOrdenCompra.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];

                        detalleOrdenCompra.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        detalleOrdenCompra.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        detalleOrdenCompraList.Add(detalleOrdenCompra);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return detalleOrdenCompraList;
        }
        #endregion

        #region GetDetalleOrdenCompra(int id)
        public ExternalWS.Entity.Global.DetalleOrdenCompra GetDetalleOrdenCompra(int id)
        {
            List<ExternalWS.Entity.Global.DetalleOrdenCompra> detalleOrdenCompraList = new List<ExternalWS.Entity.Global.DetalleOrdenCompra>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_DETALLE_ORDEN_COMPRA_BY_ID);
            cmd.AddInParameter("@id", DbType.Int32, id);
            ExternalWS.Entity.Global.DetalleOrdenCompra detalleOrdenCompra = null;
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CANTIDAD = rdr.GetOrdinal("cantidad");
                    int PRECIO_UNITARIO = rdr.GetOrdinal("precio_unitario");
                    int ID_PRODUCTO = rdr.GetOrdinal("id_producto");
                    int CODIGO_ORDEN_COMPRA = rdr.GetOrdinal("codigo_orden_compra");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    while (rdr.Read())
                    {
                        detalleOrdenCompra = new ExternalWS.Entity.Global.DetalleOrdenCompra();
                        detalleOrdenCompra.cantidad = rdr[CANTIDAD] is DBNull ? 0 : (int)rdr[CANTIDAD];
                        detalleOrdenCompra.precioUnitario = rdr[PRECIO_UNITARIO] is DBNull ? 0 : (int)rdr[PRECIO_UNITARIO];
                        detalleOrdenCompra.idProducto = rdr[ID_PRODUCTO] is DBNull ? 0 : (int)rdr[ID_PRODUCTO];
                        detalleOrdenCompra.codOrdenCompra = rdr[CODIGO_ORDEN_COMPRA] is DBNull ? string.Empty : (string)rdr[CODIGO_ORDEN_COMPRA];
                        detalleOrdenCompra.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        detalleOrdenCompra.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        detalleOrdenCompra.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);

                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return detalleOrdenCompra;
        }
        #endregion

        #region Constants
        private const string SP_INSERT_CABECERA_COTIZACION = "Global.insert_cabecera_cotizacion";
        private const string SP_UPDATE_CABECERA_COTIZACION = "Global.update_cabecera_cotizacion";
        private const string SP_DELETE_CABECERA_COTIZACION = "Global.delete_cabecera_cotizacion";
        private const string GET_CABECERA_COTIZACION_ALL = "Global.get_cabecera_cotizacion_all";
        private const string GET_CABECERA_COTIZACION_BY_ID = "Global.get_cabecera_cotizacion_by_id";
        private const string GET_CABECERA_COTIZACION_BY_DYNAMIC_PARAMETERS = "Global.get_cabecera_cotizacion_by_dynamic_parameters";
        private const string GET_CABECERA_COTIZACION_BY_CODIGO_COTIZACION = "Global.get_cabecera_cotizacion_by_codigo_cotizacion";
        private const string GET_CABECERA_COTIZACION_BY_FECHA = "Global.get_cabecera_cotizacion_by_fecha";
        private const string GET_CABECERA_COTIZACION_BY_RUN_PROVEEDOR = "Global.get_cabecera_cotizacion_by_run_proveedor";
        #endregion

        #region InsertCabeceraCotizacion(ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion)
        public void InsertCabeceraCotizacion(ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_INSERT_CABECERA_COTIZACION);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);


            cmd.AddInParameter("codigo_cotizacion", DbType.String, cabeceraCotizacion.codCotizacion);
            cmd.AddInParameter("fecha", DbType.DateTime, cabeceraCotizacion.fecha == DateTime.MinValue ? null : (object)cabeceraCotizacion.fecha);
            cmd.AddInParameter("run_proveedor", DbType.String, cabeceraCotizacion.runProveedor); cmd.AddOutParameter(PARAM_OID, DbType.Int32, 0);
            cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                cabeceraCotizacion.Id = (int)cmd.GetParameterValue(PARAM_OID);
                cabeceraCotizacion.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region UpdateCabeceraCotizacion(ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion)
        public void UpdateCabeceraCotizacion(ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_UPDATE_CABECERA_COTIZACION);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);

            cmd.AddInParameter(PARAM_ID, DbType.Int32, cabeceraCotizacion.Id);
            cmd.AddInParameter("codigo_cotizacion", DbType.String, cabeceraCotizacion.codCotizacion);
            cmd.AddInParameter("fecha", DbType.DateTime, cabeceraCotizacion.fecha == DateTime.MinValue ? null : (object)cabeceraCotizacion.fecha);
            cmd.AddInParameter("run_proveedor", DbType.String, cabeceraCotizacion.runProveedor);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(cabeceraCotizacion.TimeStamp as string));
            //cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                //cabeceraCotizacion.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region DeleteCabeceraCotizacion(ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion)
        public void DeleteCabeceraCotizacion(ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_DELETE_CABECERA_COTIZACION);
            cmd.AddInParameter(PARAM_ID, DbType.Int32, cabeceraCotizacion.Id);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);
            //cmd.AddInParameter(PARAM_IS_DELETED, DbType.Boolean, 1);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(cabeceraCotizacion.TimeStamp as string));
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                mt.AcceptTransaction();
            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region GetCabeceraCotizacion(ExternalWS.Entity.Global.FilterCabeceraCotizacion filterCabeceraCotizacion)
        public List<ExternalWS.Entity.Global.CabeceraCotizacion> GetCabeceraCotizacion(ExternalWS.Entity.Global.FilterCabeceraCotizacion filterCabeceraCotizacion)
        {
            List<ExternalWS.Entity.Global.CabeceraCotizacion> cabeceraCotizacionList = new List<ExternalWS.Entity.Global.CabeceraCotizacion>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = null;
            CabeceraCotizacionFilterType filterType = GetCabeceraCotizacionFilterType(filterCabeceraCotizacion);
            switch (filterType)
            {
                case CabeceraCotizacionFilterType.None:
                    throw new Exception("Empty Filter");
                case CabeceraCotizacionFilterType.codCotizacion:
                    cmd = mt.CreateCommand(GET_CABECERA_COTIZACION_BY_CODIGO_COTIZACION);
                    cmd.AddInParameter("codigo_cotizacion", DbType.String, filterCabeceraCotizacion.codCotizacion);
                    break;

                case CabeceraCotizacionFilterType.fecha:
                    cmd = mt.CreateCommand(GET_CABECERA_COTIZACION_BY_FECHA);
                    cmd.AddInParameter("fecha", DbType.DateTime, filterCabeceraCotizacion.fecha == DateTime.MinValue ? null : (object)filterCabeceraCotizacion.fecha);
                    break;

                case CabeceraCotizacionFilterType.runProveedor:
                    cmd = mt.CreateCommand(GET_CABECERA_COTIZACION_BY_RUN_PROVEEDOR);
                    cmd.AddInParameter("run_proveedor", DbType.String, filterCabeceraCotizacion.runProveedor);
                    break;

                default:
                    cmd = mt.CreateCommand(GET_CABECERA_COTIZACION_BY_DYNAMIC_PARAMETERS);
                    cmd.AddInParameter("@id", DbType.Int32, 0);

                    cmd.AddInParameter("codigo_cotizacion", DbType.String, filterCabeceraCotizacion.codCotizacion == "" ? null : filterCabeceraCotizacion.codCotizacion);
                    cmd.AddInParameter("fecha", DbType.DateTime, filterCabeceraCotizacion.fecha == DateTime.MinValue ? null : (object)filterCabeceraCotizacion.fecha);
                    cmd.AddInParameter("run_proveedor", DbType.String, filterCabeceraCotizacion.runProveedor == "" ? null : filterCabeceraCotizacion.runProveedor); break;
            }

            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CODIGO_COTIZACION = rdr.GetOrdinal("codigo_cotizacion");
                    int FECHA = rdr.GetOrdinal("fecha");
                    int RUN_PROVEEDOR = rdr.GetOrdinal("run_proveedor");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion = null;
                    while (rdr.Read())
                    {
                        cabeceraCotizacion = new ExternalWS.Entity.Global.CabeceraCotizacion();
                        cabeceraCotizacion.codCotizacion = rdr[CODIGO_COTIZACION] is DBNull ? string.Empty : (string)rdr[CODIGO_COTIZACION];
                        cabeceraCotizacion.fecha = rdr[FECHA] is DBNull ? DateTime.MinValue : (DateTime)rdr[FECHA];
                        cabeceraCotizacion.runProveedor = rdr[RUN_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[RUN_PROVEEDOR];
                        cabeceraCotizacion.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        cabeceraCotizacion.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        cabeceraCotizacion.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        cabeceraCotizacionList.Add(cabeceraCotizacion);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return cabeceraCotizacionList;
        }

        [Flags]
        enum CabeceraCotizacionFilterType : long
        {
            None = 1,
            codCotizacion = 4,
            fecha = 8,
            runProveedor = 16,
        }

        CabeceraCotizacionFilterType GetCabeceraCotizacionFilterType(FilterCabeceraCotizacion filterCabeceraCotizacion)
        {
            if (filterCabeceraCotizacion == null) return CabeceraCotizacionFilterType.None;
            CabeceraCotizacionFilterType filterType = new CabeceraCotizacionFilterType();
            if (filterCabeceraCotizacion.codCotizacion != null && filterCabeceraCotizacion.codCotizacion.Length > 0) filterType |= CabeceraCotizacionFilterType.codCotizacion;
            if (filterCabeceraCotizacion.fecha != DateTime.MinValue) filterType |= CabeceraCotizacionFilterType.fecha;
            if (filterCabeceraCotizacion.runProveedor != null && filterCabeceraCotizacion.runProveedor.Length > 0) filterType |= CabeceraCotizacionFilterType.runProveedor;
            return filterType;
        }

        #endregion

        #region GetCabeceraCotizacion()
        public List<ExternalWS.Entity.Global.CabeceraCotizacion> GetCabeceraCotizacion()
        {

            List<ExternalWS.Entity.Global.CabeceraCotizacion> cabeceraCotizacionList = null;
            cabeceraCotizacionList = new List<ExternalWS.Entity.Global.CabeceraCotizacion>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_CABECERA_COTIZACION_ALL);
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CODIGO_COTIZACION = rdr.GetOrdinal("codigo_cotizacion");
                    int FECHA = rdr.GetOrdinal("fecha");
                    int RUN_PROVEEDOR = rdr.GetOrdinal("run_proveedor");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");

                    ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion = null;
                    while (rdr.Read())
                    {
                        cabeceraCotizacion = new ExternalWS.Entity.Global.CabeceraCotizacion();
                        cabeceraCotizacion.codCotizacion = rdr[CODIGO_COTIZACION] is DBNull ? string.Empty : (string)rdr[CODIGO_COTIZACION];
                        cabeceraCotizacion.fecha = rdr[FECHA] is DBNull ? DateTime.MinValue : (DateTime)rdr[FECHA];
                        cabeceraCotizacion.runProveedor = rdr[RUN_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[RUN_PROVEEDOR];
                        cabeceraCotizacion.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];

                        cabeceraCotizacion.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        cabeceraCotizacion.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        cabeceraCotizacionList.Add(cabeceraCotizacion);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return cabeceraCotizacionList;
        }
        #endregion

        #region GetCabeceraCotizacion(int id)
        public ExternalWS.Entity.Global.CabeceraCotizacion GetCabeceraCotizacion(int id)
        {
            List<ExternalWS.Entity.Global.CabeceraCotizacion> cabeceraCotizacionList = new List<ExternalWS.Entity.Global.CabeceraCotizacion>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_CABECERA_COTIZACION_BY_ID);
            cmd.AddInParameter("@id", DbType.Int32, id);
            ExternalWS.Entity.Global.CabeceraCotizacion cabeceraCotizacion = null;
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CODIGO_COTIZACION = rdr.GetOrdinal("codigo_cotizacion");
                    int FECHA = rdr.GetOrdinal("fecha");
                    int RUN_PROVEEDOR = rdr.GetOrdinal("run_proveedor");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    while (rdr.Read())
                    {
                        cabeceraCotizacion = new ExternalWS.Entity.Global.CabeceraCotizacion();
                        cabeceraCotizacion.codCotizacion = rdr[CODIGO_COTIZACION] is DBNull ? string.Empty : (string)rdr[CODIGO_COTIZACION];
                        cabeceraCotizacion.fecha = rdr[FECHA] is DBNull ? DateTime.MinValue : (DateTime)rdr[FECHA];
                        cabeceraCotizacion.runProveedor = rdr[RUN_PROVEEDOR] is DBNull ? string.Empty : (string)rdr[RUN_PROVEEDOR];
                        cabeceraCotizacion.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        cabeceraCotizacion.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        cabeceraCotizacion.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);

                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return cabeceraCotizacion;
        }
        #endregion

        #region Constants
        private const string SP_INSERT_DETALLE_COTIZACION = "Global.insert_detalle_cotizacion";
        private const string SP_UPDATE_DETALLE_COTIZACION = "Global.update_detalle_cotizacion";
        private const string SP_DELETE_DETALLE_COTIZACION = "Global.delete_detalle_cotizacion";
        private const string GET_DETALLE_COTIZACION_ALL = "Global.get_detalle_cotizacion_all";
        private const string GET_DETALLE_COTIZACION_BY_ID = "Global.get_detalle_cotizacion_by_id";
        private const string GET_DETALLE_COTIZACION_BY_DYNAMIC_PARAMETERS = "Global.get_detalle_cotizacion_by_dynamic_parameters";
        private const string GET_DETALLE_COTIZACION_BY_CANTIDAD = "Global.get_detalle_cotizacion_by_cantidad";
        private const string GET_DETALLE_COTIZACION_BY_PRECIO_UNITARIO = "Global.get_detalle_cotizacion_by_precio_unitario";
        private const string GET_DETALLE_COTIZACION_BY_ID_PRODUCTO = "Global.get_detalle_cotizacion_by_id_producto";
        private const string GET_DETALLE_COTIZACION_BY_CODIGO_COTIZACION = "Global.get_detalle_cotizacion_by_codigo_cotizacion";
        #endregion

        #region InsertDetalleCotizacion(ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion)
        public void InsertDetalleCotizacion(ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_INSERT_DETALLE_COTIZACION);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);


            cmd.AddInParameter("cantidad", DbType.Int32, detalleCotizacion.cantidad == 0 ? null : (object)detalleCotizacion.cantidad);
            cmd.AddInParameter("precio_unitario", DbType.Int32, detalleCotizacion.precioUnitario == 0 ? null : (object)detalleCotizacion.precioUnitario);
            cmd.AddInParameter("id_producto", DbType.Int32, detalleCotizacion.idProducto == 0 ? null : (object)detalleCotizacion.idProducto);
            cmd.AddInParameter("codigo_cotizacion", DbType.String, detalleCotizacion.codCotizacion); cmd.AddOutParameter(PARAM_OID, DbType.Int32, 0);
            cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                detalleCotizacion.Id = (int)cmd.GetParameterValue(PARAM_OID);
                detalleCotizacion.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region UpdateDetalleCotizacion(ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion)
        public void UpdateDetalleCotizacion(ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_UPDATE_DETALLE_COTIZACION);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);

            cmd.AddInParameter(PARAM_ID, DbType.Int32, detalleCotizacion.Id);
            cmd.AddInParameter("cantidad", DbType.Int32, detalleCotizacion.cantidad == 0 ? null : (object)detalleCotizacion.cantidad);
            cmd.AddInParameter("precio_unitario", DbType.Int32, detalleCotizacion.precioUnitario == 0 ? null : (object)detalleCotizacion.precioUnitario);
            cmd.AddInParameter("id_producto", DbType.Int32, detalleCotizacion.idProducto == 0 ? null : (object)detalleCotizacion.idProducto);
            cmd.AddInParameter("codigo_cotizacion", DbType.String, detalleCotizacion.codCotizacion);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(detalleCotizacion.TimeStamp as string));
            //cmd.AddOutParameter(PARAM_TIMESTAMP, DbType.Binary, 8);
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                //detalleCotizacion.TimeStamp = Util.TimeStampToString((byte[])cmd.GetParameterValue(PARAM_TIMESTAMP));
                mt.AcceptTransaction();

            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region DeleteDetalleCotizacion(ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion)
        public void DeleteDetalleCotizacion(ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            DBCommandWrapper cmd = mt.CreateCommand(SP_DELETE_DETALLE_COTIZACION);
            cmd.AddInParameter(PARAM_ID, DbType.Int32, detalleCotizacion.Id);
            cmd.AddInParameter("creation_date", DbType.DateTime, DateTime.Now);
            //cmd.AddInParameter(PARAM_IS_DELETED, DbType.Boolean, 1);
            //cmd.AddInParameter(PARAM_TIMESTAMP, DbType.Binary, Util.StringToTimeStamp(detalleCotizacion.TimeStamp as string));
            mt.BeginTransaction();
            try
            {
                mt.ExecuteStoreProcedure(cmd);
                mt.AcceptTransaction();
            }
            catch (Exception ex)
            {
                mt.RollBackTransaction();
                throw ex;
            }

        }
        #endregion

        #region GetDetalleCotizacion(ExternalWS.Entity.Global.FilterDetalleCotizacion filterDetalleCotizacion)
        public List<ExternalWS.Entity.Global.DetalleCotizacion> GetDetalleCotizacion(ExternalWS.Entity.Global.FilterDetalleCotizacion filterDetalleCotizacion)
        {
            List<ExternalWS.Entity.Global.DetalleCotizacion> detalleCotizacionList = new List<ExternalWS.Entity.Global.DetalleCotizacion>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = null;
            DetalleCotizacionFilterType filterType = GetDetalleCotizacionFilterType(filterDetalleCotizacion);
            switch (filterType)
            {
                case DetalleCotizacionFilterType.None:
                    throw new Exception("Empty Filter");
                case DetalleCotizacionFilterType.cantidad:
                    cmd = mt.CreateCommand(GET_DETALLE_COTIZACION_BY_CANTIDAD);
                    cmd.AddInParameter("cantidad", DbType.Int32, filterDetalleCotizacion.cantidad);
                    break;

                case DetalleCotizacionFilterType.precioUnitario:
                    cmd = mt.CreateCommand(GET_DETALLE_COTIZACION_BY_PRECIO_UNITARIO);
                    cmd.AddInParameter("precio_unitario", DbType.Int32, filterDetalleCotizacion.precioUnitario);
                    break;

                case DetalleCotizacionFilterType.idProducto:
                    cmd = mt.CreateCommand(GET_DETALLE_COTIZACION_BY_ID_PRODUCTO);
                    cmd.AddInParameter("id_producto", DbType.Int32, filterDetalleCotizacion.idProducto);
                    break;

                case DetalleCotizacionFilterType.codCotizacion:
                    cmd = mt.CreateCommand(GET_DETALLE_COTIZACION_BY_CODIGO_COTIZACION);
                    cmd.AddInParameter("codigo_cotizacion", DbType.String, filterDetalleCotizacion.codCotizacion);
                    break;

                default:
                    cmd = mt.CreateCommand(GET_DETALLE_COTIZACION_BY_DYNAMIC_PARAMETERS);
                    cmd.AddInParameter("@id", DbType.Int32, 0);

                    cmd.AddInParameter("cantidad", DbType.Int32, filterDetalleCotizacion.cantidad);
                    cmd.AddInParameter("precio_unitario", DbType.Int32, filterDetalleCotizacion.precioUnitario);
                    cmd.AddInParameter("id_producto", DbType.Int32, filterDetalleCotizacion.idProducto);
                    cmd.AddInParameter("codigo_cotizacion", DbType.String, filterDetalleCotizacion.codCotizacion == "" ? null : filterDetalleCotizacion.codCotizacion); break;
            }

            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CANTIDAD = rdr.GetOrdinal("cantidad");
                    int PRECIO_UNITARIO = rdr.GetOrdinal("precio_unitario");
                    int ID_PRODUCTO = rdr.GetOrdinal("id_producto");
                    int CODIGO_COTIZACION = rdr.GetOrdinal("codigo_cotizacion");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion = null;
                    while (rdr.Read())
                    {
                        detalleCotizacion = new ExternalWS.Entity.Global.DetalleCotizacion();
                        detalleCotizacion.cantidad = rdr[CANTIDAD] is DBNull ? 0 : (int)rdr[CANTIDAD];
                        detalleCotizacion.precioUnitario = rdr[PRECIO_UNITARIO] is DBNull ? 0 : (int)rdr[PRECIO_UNITARIO];
                        detalleCotizacion.idProducto = rdr[ID_PRODUCTO] is DBNull ? 0 : (int)rdr[ID_PRODUCTO];
                        detalleCotizacion.codCotizacion = rdr[CODIGO_COTIZACION] is DBNull ? string.Empty : (string)rdr[CODIGO_COTIZACION];
                        detalleCotizacion.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        detalleCotizacion.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        detalleCotizacion.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        detalleCotizacionList.Add(detalleCotizacion);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return detalleCotizacionList;
        }

        [Flags]
        enum DetalleCotizacionFilterType : long
        {
            None = 1,
            cantidad = 4,
            precioUnitario = 8,
            idProducto = 16,
            codCotizacion = 32,
        }

        DetalleCotizacionFilterType GetDetalleCotizacionFilterType(FilterDetalleCotizacion filterDetalleCotizacion)
        {
            if (filterDetalleCotizacion == null) return DetalleCotizacionFilterType.None;
            DetalleCotizacionFilterType filterType = new DetalleCotizacionFilterType();
            if (filterDetalleCotizacion.cantidad > 0) filterType |= DetalleCotizacionFilterType.cantidad;
            if (filterDetalleCotizacion.precioUnitario > 0) filterType |= DetalleCotizacionFilterType.precioUnitario;
            if (filterDetalleCotizacion.idProducto > 0) filterType |= DetalleCotizacionFilterType.idProducto;
            if (filterDetalleCotizacion.codCotizacion != null && filterDetalleCotizacion.codCotizacion.Length > 0) filterType |= DetalleCotizacionFilterType.codCotizacion;
            return filterType;
        }

        #endregion

        #region GetDetalleCotizacion()
        public List<ExternalWS.Entity.Global.DetalleCotizacion> GetDetalleCotizacion()
        {

            List<ExternalWS.Entity.Global.DetalleCotizacion> detalleCotizacionList = null;
            detalleCotizacionList = new List<ExternalWS.Entity.Global.DetalleCotizacion>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_DETALLE_COTIZACION_ALL);
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CANTIDAD = rdr.GetOrdinal("cantidad");
                    int PRECIO_UNITARIO = rdr.GetOrdinal("precio_unitario");
                    int ID_PRODUCTO = rdr.GetOrdinal("id_producto");
                    int CODIGO_COTIZACION = rdr.GetOrdinal("codigo_cotizacion");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");

                    ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion = null;
                    while (rdr.Read())
                    {
                        detalleCotizacion = new ExternalWS.Entity.Global.DetalleCotizacion();
                        detalleCotizacion.cantidad = rdr[CANTIDAD] is DBNull ? 0 : (int)rdr[CANTIDAD];
                        detalleCotizacion.precioUnitario = rdr[PRECIO_UNITARIO] is DBNull ? 0 : (int)rdr[PRECIO_UNITARIO];
                        detalleCotizacion.idProducto = rdr[ID_PRODUCTO] is DBNull ? 0 : (int)rdr[ID_PRODUCTO];
                        detalleCotizacion.codCotizacion = rdr[CODIGO_COTIZACION] is DBNull ? string.Empty : (string)rdr[CODIGO_COTIZACION];
                        detalleCotizacion.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];

                        detalleCotizacion.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        detalleCotizacion.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);
                        detalleCotizacionList.Add(detalleCotizacion);
                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return detalleCotizacionList;
        }
        #endregion

        #region GetDetalleCotizacion(int id)
        public ExternalWS.Entity.Global.DetalleCotizacion GetDetalleCotizacion(int id)
        {
            List<ExternalWS.Entity.Global.DetalleCotizacion> detalleCotizacionList = new List<ExternalWS.Entity.Global.DetalleCotizacion>();
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.DBCommandWrapper cmd = mt.CreateCommand(GET_DETALLE_COTIZACION_BY_ID);
            cmd.AddInParameter("@id", DbType.Int32, id);
            ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion = null;
            using (IDataReader rdr = mt.ReadData(cmd))
            {
                try
                {
                    int CANTIDAD = rdr.GetOrdinal("cantidad");
                    int PRECIO_UNITARIO = rdr.GetOrdinal("precio_unitario");
                    int ID_PRODUCTO = rdr.GetOrdinal("id_producto");
                    int CODIGO_COTIZACION = rdr.GetOrdinal("codigo_cotizacion");
                    int ID = rdr.GetOrdinal("id");
                    int TID = rdr.GetOrdinal("tid");
                    int CREATION_DATE = rdr.GetOrdinal("creation_date");


                    while (rdr.Read())
                    {
                        detalleCotizacion = new ExternalWS.Entity.Global.DetalleCotizacion();
                        detalleCotizacion.cantidad = rdr[CANTIDAD] is DBNull ? 0 : (int)rdr[CANTIDAD];
                        detalleCotizacion.precioUnitario = rdr[PRECIO_UNITARIO] is DBNull ? 0 : (int)rdr[PRECIO_UNITARIO];
                        detalleCotizacion.idProducto = rdr[ID_PRODUCTO] is DBNull ? 0 : (int)rdr[ID_PRODUCTO];
                        detalleCotizacion.codCotizacion = rdr[CODIGO_COTIZACION] is DBNull ? string.Empty : (string)rdr[CODIGO_COTIZACION];
                        detalleCotizacion.Id = rdr[ID] is DBNull ? 0 : (int)rdr[ID];
                        detalleCotizacion.CreationDate = rdr[CREATION_DATE] is DBNull ? DateTime.MinValue : (DateTime)rdr[CREATION_DATE];
                        detalleCotizacion.TimeStamp = ExternalWS.Utils.Util.TimeStampToString((byte[])rdr[TID]);

                    }
                }
                finally
                {
                    rdr.Close();
                }

            }
            return detalleCotizacion;
        }
        #endregion
    }

}