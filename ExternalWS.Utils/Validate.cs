﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExternalWS.Services
{
    public class Validate
    {
        public static ExternalWS.DataContracts.ResponseTransaction ValidateRequest(ExternalWS.DataContracts.RequestParameterProducto requestParameter)
        {
            //if (requestParameter.Autentication == null)
            //{
            //    return new DataContracts.ResponseTransaction()
            //    {
            //        Code = (int)ExternalWS.Entity.WSException.EmptyAutenticationParameter,
            //        Description = "Debe ingresar los parámetros de autentificación"
            //    };
            //}
            if (requestParameter.Autentication.SystemName == null || requestParameter.Autentication.SystemName == string.Empty)
            {
                return new DataContracts.ResponseTransaction()
                {
                    Code = (int)ExternalWS.Entity.WSException.EmptyAutenticationParameter,
                    Description = "Debe ingresar el Nombre de Usuario"
                };
            }
            else if (requestParameter.Autentication.Password == null || requestParameter.Autentication.Password == string.Empty)
            {
                return new DataContracts.ResponseTransaction()
                {
                    Code = (int)ExternalWS.Entity.WSException.EmptyAutenticationParameter,
                    Description = "Debe ingresar la contraseña"
                };
            }
            else if (requestParameter.FilterProducto.IdProducto < 1)
            {
                return new DataContracts.ResponseTransaction()
                {
                    Code = (int)ExternalWS.Entity.WSException.EmptyAttribute,
                    Description = "Debe ingresar un ID mayor que 0"
                };
            }
            else if (requestParameter.FilterProducto.Descripcion == null || requestParameter.FilterProducto.Descripcion == string.Empty)
            {
                return new DataContracts.ResponseTransaction()
                {
                    Code = (int)ExternalWS.Entity.WSException.EmptyAttribute,
                    Description = "Debe ingresar la descripcion del producto"
                };
            }
            
            return new DataContracts.ResponseTransaction();
        }
    }
}