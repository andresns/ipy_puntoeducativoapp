﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.DataContracts
{
    [DataContract]
    public class CabeceraOrdenCompra
    {
        string _codOrdenCompra = "";
        string _runCliente = "";
        string _nombreCliente = "";
        string _direcionCliente = "";
        int _fonoCliente = 0;
        string _emailCliente = "";
        DateTime _fecha;

        [DataMember]
        public string CodOrdenCompra
        {
            get { return _codOrdenCompra; }
            set { _codOrdenCompra = value; }
        }

        [DataMember]
        public string RunCliente
        {
            get { return _runCliente; }
            set { _runCliente = value; }
        }

        [DataMember]
        public string NombreCliente
        {
            get { return _nombreCliente; }
            set { _nombreCliente = value; }
        }

        [DataMember]
        public string DireccionCliente
        {
            get { return _direcionCliente; }
            set { _direcionCliente = value; }
        }

        [DataMember]
        public int FonoCliente
        {
            get { return _fonoCliente; }
            set { _fonoCliente = value; }
        }

        [DataMember]
        public string EmailCliente
        {
            get { return _emailCliente; }
            set { _emailCliente = value; }
        }

        [DataMember]
        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }
    }

    [DataContract]
    public class FilterCabeceraOrdenCompra
    {
        string _codOrdenCompra = "";
        string _runCliente = "";
        string _nombreCliente = "";
        string _direcionCliente = "";
        int _fonoCliente = 0;
        string _emailCliente = "";
        DateTime _fecha;

        [DataMember]
        public string CodOrdenCompra
        {
            get { return _codOrdenCompra; }
            set { _codOrdenCompra = value; }
        }

        [DataMember]
        public string RunCliente
        {
            get { return _runCliente; }
            set { _runCliente = value; }
        }

        [DataMember]
        public string NombreCliente
        {
            get { return _nombreCliente; }
            set { _nombreCliente = value; }
        }

        [DataMember]
        public string DireccionCliente
        {
            get { return _direcionCliente; }
            set { _direcionCliente = value; }
        }

        [DataMember]
        public int FonoCliente
        {
            get { return _fonoCliente; }
            set { _fonoCliente = value; }
        }

        [DataMember]
        public string EmailCliente
        {
            get { return _emailCliente; }
            set { _emailCliente = value; }
        }

        [DataMember]
        public DateTime Fecha
        {
            get { return _fecha; }
            set { _fecha = value; }
        }
    }
}
