﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Business.Global
{
    public class Person
    {

        public void SavePerson(ExternalWS.Entity.Global.Person person, int staffMemberIdCreate)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            mt.BeginTransaction();
            try
            {
                IData.IGlobal dalPerson = Factory.Global.Create();

                switch (Utils.Utils.ChooseSaveAction(person))
                {
                    case Utils.Utils.SaveActions.Create:
                        if (Utils.Utils.NeedSave(person))
                        {
                            dalPerson.InsertPerson(person);
                        }
                        break;
                    case Utils.Utils.SaveActions.Update:
                        if (Utils.Utils.NeedSave(person))
                        {
                            dalPerson.UpdatePerson(person);
                        }
                        break;

                    case Utils.Utils.SaveActions.Delete:
                        if (Utils.Utils.NeedSave(person))
                        {
                            dalPerson.DeletePerson(person);
                        }
                        break;
                    default:
                        break;


                }
                mt.AcceptTransaction();
            }
            catch (Exception e)
            {
                mt.RollBackTransaction();
                throw e;
            }
        }

        public List<ExternalWS.Entity.Global.Person> GetPerson(ExternalWS.Entity.Global.FilterPerson filterPerson)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalPerson = Factory.Global.Create();
            List<ExternalWS.Entity.Global.Person> personList = dalPerson.GetPerson(filterPerson);
            return personList;
        }

        public List<ExternalWS.Entity.Global.Person> GetPerson()
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalPerson = Factory.Global.Create();
            List<ExternalWS.Entity.Global.Person> personList = dalPerson.GetPerson();
            return personList;
        }

        public ExternalWS.Entity.Global.Person GetPerson(int id)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalPerson = Factory.Global.Create();
            ExternalWS.Entity.Global.Person personList = dalPerson.GetPerson(id);
            return personList;
        }
    }
}