﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.DataContracts
{
    [DataContract]
    public class Proveedor
    {
        string _runProveedor = "";
        string _nombreProveedor = "";
        string _direcionProveedor = "";
        int _fonoProveedor = 0;
        string _emailProveedor = "";

        [DataMember]
        public string RunProveedor
        {
            get { return _runProveedor; }
            set { _runProveedor = value; }
        }

        [DataMember]
        public string NombreProveedor
        {
            get { return _nombreProveedor; }
            set { _nombreProveedor = value; }
        }

        [DataMember]
        public string DireccionProveedor
        {
            get { return _direcionProveedor; }
            set { _direcionProveedor = value; }
        }

        [DataMember]
        public int FonoProveedor
        {
            get { return _fonoProveedor; }
            set { _fonoProveedor = value; }
        }

        [DataMember]
        public string EmailProveedor
        {
            get { return _emailProveedor; }
            set { _emailProveedor = value; }
        }
    }

    [DataContract]
    public class FilterProveedor
    {
        string _runProveedor = "";
        string _nombreProveedor = "";
        string _direcionProveedor = "";
        int _fonoProveedor = 0;
        string _emailProveedor = "";

        [DataMember]
        public string RunProveedor
        {
            get { return _runProveedor; }
            set { _runProveedor = value; }
        }

        [DataMember]
        public string NombreProveedor
        {
            get { return _nombreProveedor; }
            set { _nombreProveedor = value; }
        }

        [DataMember]
        public string DireccionProveedor
        {
            get { return _direcionProveedor; }
            set { _direcionProveedor = value; }
        }

        [DataMember]
        public int FonoProveedor
        {
            get { return _fonoProveedor; }
            set { _fonoProveedor = value; }
        }

        [DataMember]
        public string EmailProveedor
        {
            get { return _emailProveedor; }
            set { _emailProveedor = value; }
        }
    }
}
