﻿using ExternalWS.DataContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ExternalWS.Services
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IProducto" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IProducto
    {
        [OperationContract]
        ExternalWS.DataContracts.ResponseParameterProducto GetProducto(FilterProducto filterProducto);

        [OperationContract]
        void ingresarProducto(ExternalWS.Entity.Global.Producto producto);

        [OperationContract]
        void eliminarProducto(ExternalWS.Entity.Global.Producto producto);

        [OperationContract]
        void modificarProducto(ExternalWS.Entity.Global.Producto producto);
    }
}
