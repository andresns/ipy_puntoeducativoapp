﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExternalWS.Business.Global
{
    public class DetalleCotizacion
    {

        public void SaveDetalleCotizacion(ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacion, int staffMemberIdCreate)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            mt.BeginTransaction();
            try
            {
                IData.IGlobal dalDetalleCotizacion = Factory.Global.Create();

                switch (Utils.Utils.ChooseSaveAction(detalleCotizacion))
                {
                    case Utils.Utils.SaveActions.Create:
                        if (Utils.Utils.NeedSave(detalleCotizacion))
                        {
                            dalDetalleCotizacion.InsertDetalleCotizacion(detalleCotizacion);
                        }
                        break;
                    case Utils.Utils.SaveActions.Update:
                        if (Utils.Utils.NeedSave(detalleCotizacion))
                        {
                            dalDetalleCotizacion.UpdateDetalleCotizacion(detalleCotizacion);
                        }
                        break;

                    case Utils.Utils.SaveActions.Delete:
                        if (Utils.Utils.NeedSave(detalleCotizacion))
                        {
                            dalDetalleCotizacion.DeleteDetalleCotizacion(detalleCotizacion);
                        }
                        break;
                    default:
                        break;


                }
                mt.AcceptTransaction();
            }
            catch (Exception e)
            {
                mt.RollBackTransaction();
                throw e;
            }
        }

        public List<ExternalWS.Entity.Global.DetalleCotizacion> GetDetalleCotizacion(ExternalWS.Entity.Global.FilterDetalleCotizacion filterDetalleCotizacion)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalDetalleCotizacion = Factory.Global.Create();
            List<ExternalWS.Entity.Global.DetalleCotizacion> detalleCotizacionList = dalDetalleCotizacion.GetDetalleCotizacion(filterDetalleCotizacion);
            return detalleCotizacionList;
        }

        public List<ExternalWS.Entity.Global.DetalleCotizacion> GetDetalleCotizacion()
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalDetalleCotizacion = Factory.Global.Create();
            List<ExternalWS.Entity.Global.DetalleCotizacion> detalleCotizacionList = dalDetalleCotizacion.GetDetalleCotizacion();
            return detalleCotizacionList;
        }

        public ExternalWS.Entity.Global.DetalleCotizacion GetDetalleCotizacion(int id)
        {
            IData.TransactionalHandler mt = new IData.TransactionalHandler();
            IData.IGlobal dalDetalleCotizacion = Factory.Global.Create();
            ExternalWS.Entity.Global.DetalleCotizacion detalleCotizacionList = dalDetalleCotizacion.GetDetalleCotizacion(id);
            return detalleCotizacionList;
        }
    }
}